#!/usr/bin/env python
# Required for ROS to execute file as a Python script

'''
@file       LoggingNode.py
@date       2021/05/01
@brief      Logs the coordinates from the simulation / IMU / DVL 
'''

# Avoid generating .pyc files
import sys
sys.dont_write_bytecode = True

import numpy as np
import rospy

from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Float64MultiArray
from nav_msgs.msg import Odometry

import subprocess
import os 
import signal

class LoggingNode:

    # These arrays store the relevant data from incoming topics
    
    ground_truth_pose = np.array([0.0, 0.0, 0.0, 0.0], dtype = 'f')
    ground_truth_vel = np.array([0.0, 0.0, 0.0, 0.0], dtype = 'f')
    estimated_pose = np.array([0.0, 0.0, 0.0, 0.0], dtype = 'f')
    estimated_vel = np.array([0.0, 0.0, 0.0, 0.0], dtype = 'f')
    state_cov_matrix = np.array([], dtype = 'f')

    # The above three arrays are continuously appended to the corresponding store_data arrays
    
    ground_truth_pose_data = np.array([], dtype='f')
    ground_truth_vel_data = np.array([], dtype='f')
    estimated_pose_data = np.array([], dtype='f')
    estimated_vel_data = np.array([], dtype='f')
    state_cov_matrix_data = np. array([], dtype = 'f')

    def __init__(self):
        rospy.init_node("LoggingNode", anonymous=False)
        self.firstIteration = True
        self.initialX = self.initialY = self.initialZ = 0
        self.rate = rospy.Rate(10)

        # This variable stores the current_time since our passed-in KF state covariance matrix is of type Float64MultiArray which has no time stamps associated
        self.current_time = 0;

        # Get logging mode from ROS node input
        loggingMode = rospy.get_param('loggingMode')

        # Initialize different sets of subscribers based on logging mode input

        # loggingMode: RvizOnly does nothing - you can listen in on the topics already published by 
        # LocalizationNode

        if (loggingMode == 'RvizOnly'):
            pass

        # loggingMode: Numpy records the topics into Numpy arrays
        elif (loggingMode == 'Numpy'):
            rospy.Subscriber('qubo_gazebo/estimatedPoseStamped', PoseStamped, self.estimatedPoseLog, queue_size=1)
            rospy.Subscriber('qubo_gazebo/estimatedVelStamped', PoseStamped, self.estimatedVelLog, queue_size=1)
            rospy.Subscriber('qubo_gazebo/pose_gt', Odometry, self.groundTruthLog, queue_size = 1)
            rospy.Subscriber('qubo_jetson/stateCovMatrix', Float64MultiArray, self.stateCovMatrixLog, queue_size=1)

        # loggingMode: RosBag records the topics into both Numpy arrays and Rosbags
        elif (loggingMode == 'Rosbag'):
            rospy.Subscriber('qubo_gazebo/estimatedPoseStamped', PoseStamped, self.estimatedPoseLog, queue_size=1)
            rospy.Subscriber('qubo_gazebo/estimatedVelStamped', PoseStamped, self.estimatedVelLog, queue_size=1)
            rospy.Subscriber('qubo_gazebo/pose_gt', Odometry, self.groundTruthLog, queue_size = 1)
            rospy.Subscriber('qubo_jetson/stateCovMatrix', Float64MultiArray, self.stateCovMatrixLog, queue_size=1)
            self.rosbagRecord()
            
        else:
            # Shut down node if an invalid control mode is provided
            rospy.signal_shutdown('Invalid logging mode')

        
        
    def groundTruthLog(self, msg):
        if (self.firstIteration):       
            #takes the initial pose
            self.initialX = msg.pose.pose.position.x
            self.initialY = msg.pose.pose.position.y
            self.initialZ = msg.pose.pose.position.z
            self.firstIteration = False

        else:
            # Calculates subsequent poses by subtracting from the initial pose
            xPose = msg.pose.pose.position.x - self.initialX
            yPose = msg.pose.pose.position.y - self.initialY
            zPose = msg.pose.pose.position.z - self.initialZ

            self.ground_truth_pose[0] = xPose
            self.ground_truth_pose[1] = yPose
            self.ground_truth_pose[2] = zPose

            # Log the timestamp
            secs = msg.header.stamp.secs         # Seconds since epoch
            nsecs = msg.header.stamp.nsecs       # Nanoseconds since seconds
            currentTime = float(secs) + float(nsecs) / 1000000000
            self.ground_truth_pose[3] = currentTime;

            self.ground_truth_vel[0] = msg.twist.twist.linear.x;
            self.ground_truth_vel[1] = msg.twist.twist.linear.y;
            self.ground_truth_vel[2] = msg.twist.twist.linear.z;

            self.ground_truth_vel[3] = currentTime;

            self.ground_truth_pose_data = np.append(self.ground_truth_pose_data, self.ground_truth_pose)
            self.ground_truth_vel_data = np.append(self.ground_truth_vel_data, self.ground_truth_vel)

            # Saves ground truth pose data to relative path
            dirname = os.path.dirname(__file__)
            filename = os.path.join(dirname, "./logs/groundTruthPose.npy")
            np.save(filename, self.ground_truth_pose_data);

            # Saves ground truth velocity data to relative path
            filename = os.path.join(dirname, "./logs/groundTruthVel.npy")
            np.save(filename, self.ground_truth_vel_data);


    def estimatedPoseLog(self, poseStampedMsg):
        self.estimated_pose[0] = poseStampedMsg.pose.position.x
        self.estimated_pose[1] = poseStampedMsg.pose.position.y
        self.estimated_pose[2] = poseStampedMsg.pose.position.z

        # Log the timestamp
        secs = poseStampedMsg.header.stamp.secs         # Seconds since epoch
        nsecs = poseStampedMsg.header.stamp.nsecs       # Nanoseconds since seconds
        currentTime = float(secs) + float(nsecs) / 1000000000

        self.current_time = currentTime

        self.estimated_pose[3] = currentTime

        self.estimated_pose_data = np.append(self.estimated_pose_data, self.estimated_pose)

        # Saves ground truth pose data to relative path
        dirname = os.path.dirname(__file__)
        filename = os.path.join(dirname, "./logs/estimatedPose.npy")
        np.save(filename, self.estimated_pose_data)
        
    def estimatedVelLog(self, poseStampedMsg):
        self.estimated_vel[0] = poseStampedMsg.pose.position.x
        self.estimated_vel[1] = poseStampedMsg.pose.position.y
        self.estimated_vel[2] = poseStampedMsg.pose.position.z

        # Log the timestamp
        secs = poseStampedMsg.header.stamp.secs         # Seconds since epoch
        nsecs = poseStampedMsg.header.stamp.nsecs       # Nanoseconds since seconds
        currentTime = float(secs) + float(nsecs) / 1000000000

        self.estimated_vel[3] = currentTime;
        
        self.estimated_vel_data = np.append(self.estimated_vel_data, self.estimated_vel)

        # Saves ground truth pose data to relative path
        dirname = os.path.dirname(__file__)
        filename = os.path.join(dirname, "./logs/estimatedVel.npy")
        np.save(filename, self.estimated_vel_data)

    def stateCovMatrixLog(self, msg):

        for i in range(36):
            self.state_cov_matrix_data = np.append(self.state_cov_matrix_data, msg.data[i])

        # Appends the time stamp to the np array
        self.state_cov_matrix_data = np.append(self.state_cov_matrix_data, self.current_time)

        # Saves state covariance matrix data to relative path
        dirname = os.path.dirname(__file__)
        filename = os.path.join(dirname, "./logs/stateCovMatrix.npy")
        np.save(filename, self.state_cov_matrix_data)

    def rosbagRecord(self):

        # Alter path depending on where you want to save the rosbag to, and where the record.sh file is located
        record_script = "./logging/record.sh"
        record_folder = "./logs"
        
        command = "source " + record_script
        p = subprocess.Popen(command,stdin=subprocess.PIPE, shell=True, cwd=record_folder, executable='/bin/bash')
        rospy.spin()
        

if __name__ == '__main__':
    loggerNode = LoggingNode()
    rospy.spin()
