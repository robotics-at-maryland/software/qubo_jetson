#!/usr/bin/env python
# Required for ROS to execute file as a Python script

'''
@file       logger.py
@date       2021/02/28
@brief      Logs the coordinates from the simulation / IMU / DVL into txt files
'''

# Avoid generating .pyc files
import sys
sys.dont_write_bytecode = True

import numpy as np
import rospy
from std_msgs.msg import Float64MultiArray, String
import rosbag

class rosbagtest:
     def __init__(self):
          rospy.init_node("LoggingNode", anonymous=False)
          self.rate = rospy.Rate(10)
          self.imuCounter = 0;
          self.dvlf = rosbag.Bag('dvl.bag', 'w')
          self.imuf = rosbag.Bag('imu.bag', 'w')
          self.simf = rosbag.Bag('sim.bag', 'w')
          rospy.Subscriber("qubo_gazebo/poseFromIntegration", Float64MultiArray, self.imuLog, queue_size=1)
          rospy.Subscriber("qubo_gazebo/DVLPosePlot", Float64MultiArray, self.dvlLog, queue_size=1)
          rospy.Subscriber("qubo_gazebo/SimPosePlot", Float64MultiArray, self.simLog, queue_size=1)
               


          # the rosbag syntax seems to be also just self.bagName.write("your string here")
          # reminder to test if writing to bag/txt can all be done with these same functions
          
     def imuLog(self, imuMsg):
          s = String()
          try:
               self.imuCounter = self.imuCounter + 1
               if (self.imuCounter % 10 == 0):
                    self.imuf.write(str(imuMsg.data[0]) + "\n", s)
                    self.imuf.write(str(imuMsg.data[1])+ "\n", s)
                    self.imuf.write(str(imuMsg.data[2])+ "\n", s)
                    self.imuf.write("\n",s)
          finally:
               self.imuf.close()

     def dvlLog(self,dvlMsg):
          s = String()
          try:
               self.dvlf.write(str(dvlMsg.data[0]) + "\n", s)
               self.dvlf.write(str(dvlMsg.data[1])+ "\n", s)
               self.dvlf.write(str(dvlMsg.data[2])+ "\n", s)
               self.dvlf.write("\n",s)
          finally:
               self.dvlf.close()

     def simLog(self, simMsg):
          s = String()
          try:
               self.simf.write(str(simMsg.data[0]) + "\n", s)
               self.simf.write(str(simMsg.data[1])+ "\n", s)
               self.simf.write(str(simMsg.data[2])+ "\n", s)
               self.simf.write("\n", s)
          finally:
               self.simf.close()

     # reminder to add bag.close() at the end 

if __name__ == '__main__':
     loggerNode = rosbagtest()
     rospy.spin()
     