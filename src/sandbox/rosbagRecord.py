#!/usr/bin/env python

import rospy
import subprocess
import os 
import signal

class rosbagRecord:
    def __init__(self):

        self.record_script = "/home/andrewyuantw/test_ws/src/ros_robotics/record.sh"
        self.record_folder = "/home/andrewyuantw/test_ws/src/ros_robotics"
        
        command = "source " + self.record_script
        self.p = subprocess.Popen(command,stdin=subprocess.PIPE, shell=True, cwd=self.record_folder, executable='/bin/bash')
        rospy.spin()


if __name__ == '__main__':
    rospy.init_node('rosbag_record')
    rospy.loginfo(rospy.get_name() + 'start')

    try:
        rosbag_record = rosbagRecord()
    except rospy.ROSInterruptException:
        pass