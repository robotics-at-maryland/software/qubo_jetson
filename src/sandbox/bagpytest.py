
#!/usr/bin/env python
# Required for ROS to execute file as a Python script

'''
@file       logger.py
@date       2021/02/28
@brief      Logs the coordinates from the simulation / IMU / DVL into txt files
'''

# Avoid generating .pyc files
import sys
sys.dont_write_bytecode = True

import bagpy
from bagpy import bagreader
import pandas as pd 

b = bagreader('dvl.bag') 
LASER_MSG = b.message_by_topic('qubo_gazebo/DVLPosePlot')

df_laser = pd.read_csv(LASER_MSG)
