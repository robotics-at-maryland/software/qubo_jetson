#!/usr/bin/env python2

'''
@file       GValue.py
@date       2021/02/28
@brief      Node that averages the z-acceleration experienced by Qubo from just sitting in the pool
'''

import numpy as np
import rospy
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Imu
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension


class GValue:

    def __init__(self):
        rospy.init_node('GValueNode', anonymous=False)
        
        self.counter = 0
        self.total = 0.0IMUvelocityNode
        self.average = 0.0
        rospy.Subscriber("qubo_gazebo/imu", Imu, self.getG, queue_size=1)
        self.rate = rospy.Rate(10)

    def getG(self, imuMsg):

        self.total = self.total + imuMsg.linear_acceleration.z
        self.counter = self.counter + 1
        self.average = self.total / self.counter
        print(self.average)


if __name__ == '__main__':
    GValueNode = GValue()
    rospy.spin()
