
'''
@file       plotter.py
@date       2021/02/28
@brief      Plots the coordinates from the simulation / IMU / DVL to visualize the data collected
'''

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from sensor_msgs.msg import Imu

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Sim coordinates are in green, IMU coordinates are in red, DVL coordinates are in blue

f = open("poseFromSim.txt", "r")

counter = 0
x = 0
y = 0
z = 0
xStart = 0
yStart = 0
zStart = 0


for line in f:
	if (counter % 4 == 3):
		ax.plot([xStart, x], [yStart, y], [zStart, z], c='green', marker='o')
		xStart = x
		yStart = y
		zStart = z
	if (counter % 4 == 0):
		x = float(line)
	if (counter % 4 == 1):
		y = float(line)
	if (counter % 4 == 2):
		z = float(line)
	counter = counter + 1

f.close()
f = open("poseFromDVL.txt", "r")
counter = 0
x = 0
y = 0
z = 0
xStart = 0
yStart = 0
zStart = 0

for line in f:
	if (counter % 4 == 3):
		ax.plot([xStart, x], [yStart, y], [zStart, z], c='blue', marker='^')
		xStart = x
		yStart = y
		zStart = z
	if (counter % 4 == 0):
		x = float(line)
	if (counter % 4 == 1):
		y = float(line)
	if (counter % 4 == 2):
		z = float(line)
	counter = counter + 1

f.close()

f = open("poseFromIMU.txt", "r")
counter = 0
x = 0
y = 0
z = 0
xStart = 0
yStart = 0
zStart = 0

for line in f:
	if (counter % 4 == 3):
		ax.plot([xStart, x], [yStart, y], [zStart, z], c='red', marker='^')
		xStart = x
		yStart = y
		zStart = z
	if (counter % 4 == 0):
		x = float(line)
	if (counter % 4 == 1):
		y = float(line)
	if (counter % 4 == 2):
		z = float(line)
	counter = counter + 1

f.close()


ax.set_xlabel("X Axis")
ax.set_ylabel("Y Axis")
ax.set_zlabel("Z Axis")
plt.show()
