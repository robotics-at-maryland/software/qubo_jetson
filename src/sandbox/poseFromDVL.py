#!/usr/bin/env python2

'''
@file       poseFromDVL.py
@date       2021/02/28
@brief      Node that integrates the DVL data to get position
'''

import numpy as np
import rospy
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Imu
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension
from tf.transformations import euler_from_quaternion
from tf.transformations import quaternion_matrix
from geometry_msgs.msg import TwistWithCovarianceStamped
from geometry_msgs.msg import PoseStamped

class velocityFromDVL:

    velPub = None
    posePub = None
    linearVelocity = \
        np.array([0.0, 0.0, 0.0])   	# Linear velocity integrated from IMU acceleration
    previousTime = None			        # Previous timestamp
    xyzPosition = \
        np.array([0.0, 0.0, 0.0])	    # XYZ position integrated from linear velocity
    orientation = \
        np.array([0.0, 0.0, 0.0])       # roll/pitch/yaw integrated from angular velocity
    store_data = np.array([], dtype='f')



    def __init__(self):
        rospy.init_node('DVLvelocityNode', anonymous=False)
        self.posePub = rospy.Publisher('qubo_gazebo/DVLPosePlot', Float64MultiArray, queue_size = 1)
        self.poseStamped = rospy.Publisher('qubo_gazebo/dvlPoseStamped', PoseStamped, queue_size = 1)
        self.f = open("poseFromDVL.txt", "a")
        self.counter = 0
        rospy.Subscriber("qubo_gazebo/dvl_twist", TwistWithCovarianceStamped, self.getVelocity, queue_size=1)
        self.rate = rospy.Rate(30)

    def getVelocity(self, DVLMsg):
        
        # Extract message timestamp
        secs = DVLMsg.header.stamp.secs     	# Seconds since epoch
        nsecs = DVLMsg.header.stamp.nsecs   	# Nanoseconds since seconds

        currentTime = float(secs) + float(nsecs) / 1000000000

        # Calculate delta time
        if (self.previousTime == None):
            dt = 0
            self.previousTime = currentTime
        else:
            dt = currentTime - self.previousTime
            self.previousTime = currentTime
        

        zRate = -DVLMsg.twist.twist.linear.x 
        yRate = DVLMsg.twist.twist.linear.y
        xRate = DVLMsg.twist.twist.linear.z 
        # I may have gotten the roll pitch yaw/xyz thing wrong. Check later
        rollRate = DVLMsg.twist.twist.angular.x
        pitchRate = DVLMsg.twist.twist.angular.y
        yawRate = DVLMsg.twist.twist.angular.z
        
        

        # Integrate velocity to get position
        self.xyzPosition[0] += xRate * dt 	
        self.xyzPosition[1] += yRate * dt	
        self.xyzPosition[2] += zRate * dt 	

        xPose = self.xyzPosition[0]
        yPose = self.xyzPosition[1]
        zPose = self.xyzPosition[2]

    

        self.orientation[0] += rollRate * dt 	
        self.orientation[1] += pitchRate * dt	
        self.orientation[2] += yawRate * dt 	

        rollPose = self.orientation[0]
        pitchPose = self.orientation[1]
        yawPose = self.orientation[2]

        poseMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(xPose, yPose, zPose, 
            rollPose, pitchPose, yawPose)
        )

        pose = PoseStamped()
        pose.header.frame_id = "map"
        
        pose.pose.position.x = xPose
        pose.pose.position.y = yPose
        pose.pose.position.z = zPose
        pose.pose.orientation.x = 0
        pose.pose.orientation.y = 0
        pose.pose.orientation.z = -0.7
        pose.pose.orientation.w = 0.7

        self.poseStamped.publish(pose)

        
        self.store_data = np.append(self.store_data, self.xyzPosition);
        np.save("/home/andrewyuantw/record/dvlRecord.npy", self.store_data);
        
        

        self.posePub.publish(poseMsg)



if __name__ == '__main__':
    DVLvelocityNode = velocityFromDVL()
    rospy.spin()