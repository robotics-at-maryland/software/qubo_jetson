#!/usr/bin/env python
# Required for ROS to execute file as a Python script

'''
@file       ControlsNode.py
@date       2020/10/31
@brief      Qubo controls node that stabilizes 6 DOF velocity using a PID controller
'''

# Avoid generating .pyc files
import sys
sys.dont_write_bytecode = True

import rospy
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Wrench
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import Imu

import numpy as np

from controls.StateController import StateController


'''
@brief      Qubo controls ROS node
'''
class ControlsNode:
    # QUBO FRAME: X=RIGHT, Y=FORWARD, Z=UP
    # Pose is expressed in world frame
    # Velocity is expressed in Qubo frame

    # Speed limits for pose conroller output requests
    maxLinearSpeed = .5 #used to be 1             # m/s
    maxAngularSpeed = 1 #used to be  1.5     # rad/s
    
    thrusterPub = None              # Thruster commands publisher
    rate = None                     # ROS publishing rate object

    # NumPy array of desired 6 DOF pose vector
    desiredPose = \
        np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0]) 

    # NumPy array of desired 6 DOF velocity vector
    desiredVelocity = \
        np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0]) 
    
    # NumPy array of current 6 DOF velocity vector
    # Used in pose control
    currentVelocity = \
        np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
    
    # Rotation matrix from world frame to Qubo frame
    # Used in pose control
    R_qw = np.zeros((3,3))

    # Velocity and pose StateController objects
    velocityController = None
    poseController = None


    '''
    @brief      Class constructor
    @param      velocityPIDMatrix   6x3 NumPy array of PID coefficients for the velocity controller
    @param      posePIDMatrix       6x3 NumPy array of PID coefficients for the pose controller
    @return     None
    '''
    def __init__(
        self, 
        velocityPIDMatrix=np.array([
            [0.3, 0.1, 0.8],
            [0.3, 0.1, 0.8],
            [0.3, 0.1, 0.8],
            [0.3, 0.01, 0.5],
            [0.3, 0.01, 0.5],
            [0.3, 0.01, 0.5]]),
        posePIDMatrix=np.array([
            [0.3, 0.05, 0.4],
            [0.3, 0.05, 0.4],
            [0.3, 0.05, 0.4],
            [0.2, 0.01, 0.05],
            [0.2, 0.01, 0.05],
            [0.2, 0.01, 0.05]])
        ):

        # Override PID coefficients matrix inputs if ROS parameters are set for these
        if (rospy.has_param('velocityPIDMatrix')):
            velocityPIDMatrix = np.array(rospy.get_param('velocityPIDMatrix'))
        if (rospy.has_param('posePIDMatrix')):
            posePIDMatrix = np.array(rospy.get_param('posePIDMatrix'))

        # Instantiate velocity and pose StateController objects
        self.velocityController = StateController(velocityPIDMatrix)
        self.poseController = StateController(posePIDMatrix, angleFlag=True)

        # Initialize ROS node
        rospy.init_node('ControlsNode', anonymous=False)

        # Initialize thruster command publisher
        self.thrusterPub = rospy.Publisher('qubo_gazebo/thruster_manager/input', Wrench, queue_size=1)

        # Set publishing rate (10 Hz)
        self.rate = rospy.Rate(10)

        # Get control mode from ROS node input
        controlMode = rospy.get_param('controlMode')

        # Initialize different sets of subscribers based on control mode input
        if (controlMode == 'velocityPID'):
            rospy.Subscriber("qubo_jetson/desiredVelocity", Float64MultiArray, self.desiredVelocityUpdate, queue_size=1)
            rospy.Subscriber("qubo_jetson/currentVelocity", Float64MultiArray, self.controlVelocity, queue_size=1)

        elif (controlMode == 'posePID'):
            rospy.Subscriber("qubo_jetson/desiredPose", Float64MultiArray, self.desiredPoseUpdate, queue_size=1)
            rospy.Subscriber("qubo_jetson/currentVelocity", Float64MultiArray, self.currentVelUpdate, queue_size=1)
            rospy.Subscriber("qubo_jetson/currentPose", Float64MultiArray, self.controlPose, queue_size=1)
            rospy.Subscriber("qubo_jetson/R_qw", Float64MultiArray, self.rotationMatrixUpdate, queue_size=1)

        elif (controlMode == 'manual'):
            # Shut down node if manual mode is selected
            rospy.signal_shutdown('Manual mode selected')

        else:
            # Shut down node if an invalid control mode is provided
            rospy.signal_shutdown('Invalid control mode')
        

    '''
    @brief      Callback function to update the desiredPose vector
    @param      desiredPoseMsg     ROS message with desired pose values
    @return     None
    '''
    def desiredPoseUpdate(self, desiredPoseMsg):
        self.desiredPose = np.array([
            [float(desiredPoseMsg.data[0])], 
            [float(desiredPoseMsg.data[1])], 
            [float(desiredPoseMsg.data[2])], 
            [float(desiredPoseMsg.data[3])], 
            [float(desiredPoseMsg.data[4])], 
            [float(desiredPoseMsg.data[5])]])
    

    '''
    @brief      Callback function to update the desiredPose vector
    @param      desiredPoseMsg     ROS message with desired velocity values
    @return     None
    '''
    def desiredVelocityUpdate(self, desiredVelMsg):
        self.desiredVelocity = np.array([
            [float(desiredVelMsg.data[0])], 
            [float(desiredVelMsg.data[1])], 
            [float(desiredVelMsg.data[2])], 
            [float(desiredVelMsg.data[3])], 
            [float(desiredVelMsg.data[4])], 
            [float(desiredVelMsg.data[5])]])
    

    '''
    @brief      Callback function to update the currentVelocity vector
    @param      currentVelMsg       ROS message with current velocity values
    @return     None
    '''
    def currentVelUpdate(self, currentVelMsg):
        self.currentVelocity = np.array([
            [float(currentVelMsg.data[0])], 
            [float(currentVelMsg.data[1])], 
            [float(currentVelMsg.data[2])], 
            [float(currentVelMsg.data[3])], 
            [float(currentVelMsg.data[4])], 
            [float(currentVelMsg.data[5])]])
    
    
    '''
    @brief      Callback function to update the R_qw matrix
    @param      desiredPoseMsg     ROS message with desired velocity values
    @return     None
    '''
    def rotationMatrixUpdate(self, R_qwMsg):
        dim0 = R_qwMsg.layout.dim[0]
        dim1 = R_qwMsg.layout.dim[1]

        for i in range(dim0.size):
            for j in range(dim1.size):
                self.R_qw[i][j] = float(R_qwMsg.data[dim1.stride*i + j])


    '''
    @brief      Callback function to continually match desired velocity
    @param      currentVelMsg       ROS message with current velocity values
    @return     None
    '''
    def controlVelocity(self, currentVelMsg):
        # Extract current velocity vector values into an array
        currentVelocity = np.array([
            [float(currentVelMsg.data[0])], 
            [float(currentVelMsg.data[1])], 
            [float(currentVelMsg.data[2])], 
            [float(currentVelMsg.data[3])], 
            [float(currentVelMsg.data[4])], 
            [float(currentVelMsg.data[5])]])

        # Compute thruster input vector
        thrusterInput = self.velocityController.getOutput(self.desiredVelocity, currentVelocity)
        
        # Construct thruster commands message
        xInput = thrusterInput[0]
        yInput = thrusterInput[1]
        zInput = thrusterInput[2]
        rollInput = thrusterInput[3]
        pitchInput = thrusterInput[4]
        yawInput = thrusterInput[5]
        thrusterInputMsg = \
            Wrench(
                force=Vector3(x=xInput, y=yInput, z=zInput), 
		        #force = Vector3(x=xInput, y =0, z=0),
                torque=Vector3(x=pitchInput, y=rollInput, z=yawInput)
            )
        
        '''
        print('xInput: ' + str(xInput))
        print('yInput: ' + str(yInput))
        print('zInput: ' + str(zInput))
        print('---')
        '''

        # Publish thruster commands message
        self.thrusterPub.publish(thrusterInputMsg)
        
        self.rate.sleep()
    

    '''
    @brief      Callback function to continually match desired pose
    @param      currentPoseMsg      ROS message with current pose values
    @return     None
    '''
    def controlPose(self, currentPoseMsg):
        # Extract current pose vector values into an array
        currentPose = np.array([
            [float(currentPoseMsg.data[0])], 
            [float(currentPoseMsg.data[1])], 
            [float(currentPoseMsg.data[2])], 
            [float(currentPoseMsg.data[3])], 
            [float(currentPoseMsg.data[4])], 
            [float(currentPoseMsg.data[5])]])

        # Compute desired velocity vector
        desiredVelocity = self.poseController.getOutput(self.desiredPose, currentPose)

        # Constrain desired velocity values
        desiredLinearVelocity = np.clip(desiredVelocity[0:3], a_min=-self.maxLinearSpeed, a_max=self.maxLinearSpeed)
        desiredAngularVelocity = np.clip(desiredVelocity[3:6], a_min=-self.maxAngularSpeed, a_max=self.maxAngularSpeed)
        desiredVelocity = np.concatenate((desiredLinearVelocity, desiredAngularVelocity), axis=0)

        # Transform desired linear velocity component from world frame to Qubo frame
        desiredLinearVel_w = desiredVelocity[0:3]
        desiredLinearVel_q = np.matmul(self.R_qw, desiredLinearVel_w)
        desiredVelocity[0:3] = desiredLinearVel_q

        # Compute thruster input vector
        thrusterInput = self.velocityController.getOutput(desiredVelocity, self.currentVelocity)
        
        # Construct thruster commands message
        xInput = thrusterInput[0]
        yInput = thrusterInput[1]
        zInput = thrusterInput[2]
        rollInput = thrusterInput[3]
        pitchInput = thrusterInput[4]
        yawInput = thrusterInput[5]
        thrusterInputMsg = \
            Wrench(
                force=Vector3(x=xInput, y=yInput, z=zInput), 
		        #force = Vector3(x=xInput, y =0, z=0),
                torque=Vector3(x=pitchInput, y=rollInput, z=yawInput)
            )
        
        '''
        print('xInput: ' + str(xInput))
        print('yInput: ' + str(yInput))
        print('zInput: ' + str(zInput))
        print('---')
        '''

        # Publish thruster commands message
        self.thrusterPub.publish(thrusterInputMsg)
        
        self.rate.sleep()



if __name__ == '__main__':
    controlsNode = ControlsNode()
    rospy.spin()

