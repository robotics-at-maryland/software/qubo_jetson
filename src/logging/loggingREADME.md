File overview:

Record.sh is a bash script that contains a simple command to record the specified ROS topics in a rosbag

playback.launch is a launch file to play back Rosbags so we can then use Rviz to visualize the topics
To run it, type ' roslaunch folder_this_file_is_stored playback.launch file:=PATHTOROSBAG/rosbagName.bag

plotternumpy.py will plot out the points stored in .npy arrays using matplotlib

To run rviz, use command 'rosrun rviz rviz' . Towards the right, click "Add" and then click "Pose".
Expand the menu, and there should be a blank field for "Topic". 
Click on the dropdown mennu, and you should see poseStamped messages that you can then visualize. 