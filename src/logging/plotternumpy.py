
'''
@file       plotternumpy.py
@date       2021/05/01
@brief      Plots the coordinates from the simulation / IMU / DVL to visualize the data collected
'''

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from sensor_msgs.msg import Imu

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Sim coordinates are in green, IMU coordinates are in red, DVL coordinates are in blue

# replace path with path to simulation's npy file
data = np.load('../logs/groundTruthPose.npy')

counter = 0
xStart = -data[1]
yStart = data[0]
zStart = data[2]

while (counter <= len(data) - 4):
	y = data[counter + 0]
	x = -data[counter + 1]
	z = data[counter + 2]
	ax.plot([xStart, x], [yStart, y], [zStart, z], c='green', marker='^')
	xStart = x
	yStart = y
	zStart = z
	counter += 4

# replace path with path to IMU's npy file

data = np.load('../logs/estimatedPose.npy')

counter = 0
xStart = data[0]
yStart = data[1]
zStart = data[2]

while (counter <= len(data) - 4):
	x = data[counter + 0]
	y = data[counter + 1]
	z = data[counter + 2]
	ax.plot([xStart, x], [yStart, y], [zStart, z], c='red', marker='^')
	xStart = x
	yStart = y
	zStart = z
	counter += 4

ax.set_xlabel("X Axis")
ax.set_ylabel("Y Axis")
ax.set_zlabel("Z Axis")
plt.show()
