#!/#!/usr/bin/env python2


'''
@file       controlsNodePosePIDtest.py
@date       2021/1/18
@brief      Qubo controls node that stabilizes pose utilizing a PID controller
'''


import sys
sys.dont_write_bytecode = True

import rospy
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension
from tf.transformations import euler_from_quaternion
from tf.transformations import quaternion_matrix
import numpy as np
from PID import PID


class poseControlsNode:

    VelPub = None        			# Thruster commands publisher
    rate = None                 		# ROS publishing rate object
    infoFromIMU = np.array([0,0,0,0,0,0])	# Pose information integrated from the IMU

    xVelPID = PID(0.8, 0.1, 0.6)       		# X axis linear velocity PID controller
    yVelPID = PID(0.8, 0.1, 0.6)       		# Y axis linear velocity PID controller
    zVelPID = PID(0.8, 0.1, 0.6)       		# Z axis linear velocity PID controller
    rollVelPID = PID(0, 0, 0)    		# Y axis rotational velocity PID controller
    pitchVelPID = PID(0, 0, 0) 			# X axis rotational velocity PID controller
    yawVelPID = PID(0, 0, 0)  			# Z axis rotational velocity PID controller

	
    def __init__(self):
	rospy.init_node('poseControlsNode', anonymous=False)
        self.VelPub = rospy.Publisher('qubo_jetson/desiredVelocity', Float64MultiArray, queue_size=1)
	rospy.Subscriber("qubo_jetson/desiredPose", Float64MultiArray, self.desiredPoseUpdate, queue_size=1)
	rospy.Subscriber("qubo_gazebo/poseFromIntegration", Float64MultiArray, self.controlPose, queue_size=1)	
	self.rate = rospy.Rate(50)

    def desiredPoseUpdate(self, desiredPoseMsg):

        self.desiredPose = np.array([
            [float(desiredPoseMsg.data[0])], 
            [float(desiredPoseMsg.data[1])], 
            [float(desiredPoseMsg.data[2])], 
            [float(desiredPoseMsg.data[3])], 
            [float(desiredPoseMsg.data[4])], 
            [float(desiredPoseMsg.data[5])]])

    def controlPose(self, poseMsg):

	self.infoFromIMU = np.array([
            [float(poseMsg.data[0])], 
            [float(poseMsg.data[1])], 
            [float(poseMsg.data[2])], 
            [float(poseMsg.data[3])], 
            [float(poseMsg.data[4])], 
            [float(poseMsg.data[5])]])

        xPose = self.infoFromIMU[0]
        yPose = self.infoFromIMU[1]
        zPose = self.infoFromIMU[2]

	


	rollPose = self.infoFromIMU[3]
	pitchPose = self.infoFromIMU[4]
	yawPose = self.infoFromIMU[5]
	

	xRate = self.xVelPID.update(self.desiredPose[0], xPose)
        yRate = self.yVelPID.update(self.desiredPose[1], yPose)
        zRate = self.zVelPID.update(self.desiredPose[2], zPose)
        rollRate = self.rollVelPID.update(self.desiredPose[3], rollPose)
        pitchRate = self.pitchVelPID.update(self.desiredPose[4], pitchPose)
        yawRate = self.yawVelPID.update(self.desiredPose[5], yawPose)


	desiredVelMsg = Float64MultiArray(
                layout=MultiArrayLayout(
                    dim=(MultiArrayDimension(
                        label='dim1',
                        size=6,
                        stride=6
                    ),),
                    data_offset=0
                ),
                data=(xRate, yRate, zRate, 
                rollRate, pitchRate, yawRate)
            )

	self.VelPub.publish(desiredVelMsg)

	
        self.rate.sleep()

if __name__ == '__main__':
    poseControlsNode = poseControlsNode()
    rospy.spin()
