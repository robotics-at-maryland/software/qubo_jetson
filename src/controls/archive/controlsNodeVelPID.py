#!/usr/bin/env python2
# Required for ROS to execute file as a Python script

'''
@file       controlsNodeVelPID.py
@date       2020/10/31
@brief      Qubo controls node that stabilizes 6 DOF velocity using a PID controller
'''

# Avoid generating .pyc files
import sys
sys.dont_write_bytecode = True

import rospy
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Wrench
from geometry_msgs.msg import Vector3
from sensor_msgs.msg import Imu

from tf.transformations import euler_from_quaternion
from tf.transformations import quaternion_matrix

import numpy as np

from PID import PID


'''
@brief      Qubo controls ROS node
'''
class ControlsNode:
    # QUBO FRAME: X=RIGHT, Y=FORWARD, Z=UP
    
    thrusterPub = None              # Thruster commands publisher
    rate = None                     # ROS publishing rate object

    desiredVelocity = \
        np.array([0,0,0,0,0,0])     # NumPy array of desired 6 DOF velocity vector
    
    linearVelocity = \
        np.array([0.0, 0.0, 0.0])   # Linear velocity integrated from IMU acceleration

    previousTime = None             # Previous time stamp

    xPID = PID(0.5, 0.5, 0.9)       # X axis translation PID controller
    yPID = PID(0.5, 0.5, 0.9)       # Y axis translation PID controller
    zPID = PID(0.5, 0.5, 0.9)       # Z axis translation PID controller
    rollPID = PID(0.5, 0.1, 0.6)    # Y axis rotation PID controller
    pitchPID = PID(0.5, 0.1, 0.6)   # X axis rotation PID controller
    yawPID = PID(0.5, 0.1, 0.6)     # Z axis rotation PID controller


    '''
    @brief      Class constructor
    @param      None
    @return     None
    '''
    def __init__(self):
        # Initialize ROS node, publishers and subscribers
        rospy.init_node('controlsNode', anonymous=False)
        self.thrusterPub = rospy.Publisher('qubo_gazebo/thruster_manager/input', Wrench, queue_size=1)

        ### May want to create a custom message type for this in the future
        rospy.Subscriber("qubo_jetson/desiredVelocity", Float64MultiArray, self.desiredVelUpdate, queue_size=1)
        rospy.Subscriber("qubo_gazebo/imu", Imu, self.controlVelocity, queue_size=1)
        
        # Set publishing rate (10 Hz)
        self.rate = rospy.Rate(10)


    '''
    @brief      Callback function to update the desiredVelocity vector
    @param      desiredVelMsg   ROS message with desired velocity values
    @return     None
    '''
    def desiredVelUpdate(self, desiredVelMsg):
        self.desiredVelocity = np.array([
            [float(desiredVelMsg.data[0])], 
            [float(desiredVelMsg.data[1])], 
            [float(desiredVelMsg.data[2])], 
            [float(desiredVelMsg.data[3])], 
            [float(desiredVelMsg.data[4])], 
            [float(desiredVelMsg.data[5])]])


    '''
    @brief      Callback function to continually match desired velocity
    @param      velocityMsg     ROS message with current velocity values
    @return     None
    '''
    def controlVelocity(self, imuMsg):
        # Extract message timestamp
        secs = imuMsg.header.stamp.secs     # Seconds since epoch
        nsecs = imuMsg.header.stamp.nsecs    # Nanoseconds since seconds

        currentTime = float(secs) + float(nsecs) / 1000000000

        # Calculate delta time
        if (self.previousTime == None):
            dt = 0
            self.previousTime = currentTime
        else:
            dt = currentTime - self.previousTime
            self.previousTime = currentTime

        # Extract IMU orientation quaternion
        qx = imuMsg.orientation.x
        qy = imuMsg.orientation.y
        qz = imuMsg.orientation.z
        qw = imuMsg.orientation.w

        # Get rotation matrix from quaternion (world frame to Qubo frame)
        R_wq = quaternion_matrix([qx, qy, qz, qw])[0:3, 0:3]
        R_qw = R_wq.T
        #R_qw = np.linalg.inv(R_wq)

        # Convert gravitational accel vector from world to Qubo frame
        # NOTE: The IMU offset is positive for some reason; need to investigate in the future
        g_w = np.array([[0], [0], [9.81]])
        g_q = np.matmul(R_qw, g_w)
        
        # Extract IMU linear acceleration values and offset by g_q components
        xAccel = imuMsg.linear_acceleration.x - g_q[0]
        yAccel = imuMsg.linear_acceleration.y - g_q[1]
        zAccel = imuMsg.linear_acceleration.z - g_q[2]

        # Integrate accelerate to get velocity

        self.linearVelocity[0] += xAccel * dt
        self.linearVelocity[1] += yAccel * dt
        self.linearVelocity[2] += zAccel * dt

        xRate = self.linearVelocity[0]
        yRate = self.linearVelocity[1]
        zRate = self.linearVelocity[2]

        # Extract angular velocity values
        rollRate = imuMsg.angular_velocity.y
        pitchRate = imuMsg.angular_velocity.x
        yawRate = imuMsg.angular_velocity.z

        print('xRate: ' + str(xRate))
        print('yRate: ' + str(yRate))
        print('zRate: ' + str(zRate))
        print('---')

        print('Desired xRate: ' + str(self.desiredVelocity[0]))
        print('Desired yRate: ' + str(self.desiredVelocity[1]))
        print('Desired zRate: ' + str(self.desiredVelocity[2]))
        
        # Compute PID controller outputs
        xInput = self.xPID.update(self.desiredVelocity[0], xRate)
        yInput = self.yPID.update(self.desiredVelocity[1], yRate)
        zInput = self.zPID.update(self.desiredVelocity[2], zRate)
        rollInput = self.rollPID.update(self.desiredVelocity[3], rollRate)
        pitchInput = self.pitchPID.update(self.desiredVelocity[4], pitchRate)
        yawInput = self.yawPID.update(self.desiredVelocity[5], yawRate)

        # Construct thruster commands message
        thrusterInputMsg = \
            Wrench(
                force=Vector3(x=xInput, y=yInput, z=zInput), 
		        #force = Vector3(x=xInput, y =0, z=0),
                torque=Vector3(x=pitchInput, y=rollInput, z=yawInput)
            )
        
        print('xInput: ' + str(xInput))
        print('yInput: ' + str(yInput))
        print('zInput: ' + str(zInput))
        print('---')

        # Publish thruster commands message
        self.thrusterPub.publish(thrusterInputMsg)
        
        self.rate.sleep()



if __name__ == '__main__':
    controlsNode = ControlsNode()
    rospy.spin()

