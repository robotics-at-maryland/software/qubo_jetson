#!/usr/bin/env python2
# Required for ROS to execute file as a Python script

'''
@file       PoseTeleopNode.py
@date       2021/1/18
@brief      Qubo teleop node that utilizes the velocity PID controls node
'''


import time
import pygame

import rospy
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension


'''
@brief      Qubo orientation teleoperation node
'''
class poseTeleop:
    # QUBO FRAME: X=RIGHT, Y=FORWARD, Z=UP

    desiredPosePub = None        # Thruster commands publisher
    pose = None                 # ROS publishing rate object

    xPose = 0                   # Desired linear x pose
    yPose = 0                   # Desired linear y pose
    zPose = 0                   # Desired linear z pose
    rollPose = 0                # Desired roll rate
    pitchPose = 0               # Desired pitch rate
    yawPose = 0                 # Desired yaw rate
    
    i = 0
    prevKeyStates = None        # Variable to keep track of previous key press states


    '''
    @brief      Class constructor
    @param      None
    @return     None
    '''
    def __init__(self):
        # Initialize ROS node, publishers and subscribers
        rospy.init_node('poseTeleopNode', anonymous=False)
        self.desiredPosePub = rospy.Publisher('qubo_jetson/desiredPose', Float64MultiArray, queue_size=1)

        # Set publishing rate (50 Hz)
        self.rate = rospy.Rate(50)

        # Initialize PyGame
        pygame.init()
        pygame.display.set_mode([100, 100])
        pygame.key.set_repeat(10, 10)

        # Get initial key states
        self.prevKeyStates = pygame.key.get_pressed()


    '''
    @brief      Main application run function
    @param      None
    @return     None
    '''
    def teleop(self):
        # set six poses - middle of the gate, right of the pole, behind the pole, left of the pole, middle of the gate, and the starting position
        xPoses = [0,0,0,0,0,0]
        yPoses = [5,5,5,5,5,5]
        zPoses = [-1,-1,-1,-1,-1,-1]
        rollPoses = [1,0,0,-1,0,0]
        pitchPoses = [0,1,0,0,0,0]
        yawPoses = [0,0,1,0,0,0]

        self.xPose = xPoses[0]
        self.yPose = yPoses[0]
        self.zPose = zPoses[0]
        self.rollPose = rollPoses[0]
        self.pitchPose = pitchPoses[0]
        self.yawPose = yawPoses[0]
        self.index = 0

        while (rospy.is_shutdown() == False):
            
            keyStates = pygame.key.get_pressed()

            # For some reason, without this for-loop, it will not detect my key press? 
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    print event.key

            # Change to the next desired pose by pressing "W"
            # Only change a velocity when a key transitions not pressed to pressed
            if (keyStates[pygame.K_w] and self.prevKeyStates[pygame.K_w]==False):
				self.xPose = xPoses[self.index+1]
				self.yPose = yPoses[self.index+1]
				self.zPose = zPoses[self.index+1]
				self.rollPose = rollPoses[self.index+1]
				self.pitchPose = pitchPoses[self.index+1]
				self.yawPose = yawPoses[self.index+1]
				self.index = self.index +1;
		
            self.prevKeyStates = keyStates
	    
            # Construct very convoluted 6-element 1D vector ROS message
            desiredPoseMsg = Float64MultiArray(
                layout=MultiArrayLayout(
                    dim=(MultiArrayDimension(
                        label='dim1',
                        size=6,
                        stride=6
                    ),),
                    data_offset=0
                ),
                data=(self.xPose, self.yPose, self.zPose, 
                self.rollPose, self.pitchPose, self.yawPose)
            )

            # Publish desired psoe message
            print('Desired Pose: ')
            print(desiredPoseMsg.data)
            self.desiredPosePub.publish(desiredPoseMsg)
            self.rate.sleep()



if __name__ == '__main__':
    try:
        poseTeleopNode = poseTeleop()
        poseTeleopNode.teleop()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
