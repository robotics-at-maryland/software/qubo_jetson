#!/usr/bin/env python2

'''
@file	cheat.py
@date	2021/01/18
@brief 	node created to test PID controllers without having to worry about drift from integration
'''

import sys
sys.dont_write_bytecode = True


import rospy
from tempfile import TemporaryFile
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import PoseStamped
from tf.transformations import euler_from_quaternion
from tf.transformations import quaternion_multiply

import numpy as np

from PID import PID


class poseControlsNode:

    VelPub = None        			# Velocity commands publisher
    rate = None                 		# ROS publishing rate object
    plotterPub = None
    desiredPose = \
        np.array([0, 0, 0, 0, 0, 0])       	# NumPy array of desired 6 DOF pose vector

    currentPose = np.array([0, 0, 0, 0, 0, 0], dtype='f')
    store_data = np.array([], dtype='f')
    # I tried increasing the velocity PID coefficients further, but it started to become unstable
    xVelPID = PID(0.4, 0.05, 0.4)	      	# X axis linear velocity PID controller
    yVelPID = PID(0.4, 0.05, 0.4)      	 	# Y axis linear velocity PID controller
    zVelPID = PID(0.4, 0.05, 0.4)      		# Z axis linear velocity PID controller
    
    rollVelPID = PID(0.2, 0.01, 0.05)    		# Y axis rotational velocity PID controller
    pitchVelPID = PID(0.2, 0.01, 0.05)   		# X axis rotational velocity PID controller
    yawVelPID = PID(0.2, 0.01, 0.05)     		# Z axis rotational velocity PID controller

	
    def __init__(self):
        rospy.init_node('poseControlsNode', anonymous=False)
        self.VelPub = rospy.Publisher('qubo_jetson/desiredVelocity', Float64MultiArray, queue_size=1)
        rospy.Subscriber("qubo_jetson/desiredPose", Float64MultiArray, self.desiredPoseUpdate, queue_size=1)
        self.firstIteration = True
        self.initialX = self.initialY = self.initialZ = 0
        self.initialqX = self.initialqY = self.initialqZ = self.initialqW = 0
        rospy.Subscriber('qubo_gazebo/pose_gt', Odometry, self.controlPose)
        self.posePub = rospy.Publisher('qubo_gazebo/SimPosePlot', Float64MultiArray, queue_size = 1)	
        self.counter = 0
        self.rate = rospy.Rate(50)
        self.poseStamped = rospy.Publisher('qubo_gazebo/simPoseStamped', PoseStamped, queue_size=1)

    def desiredPoseUpdate(self, desiredPoseMsg):

	    #I have to flip the x and y in order for the cheat.py to work
        self.desiredPose = np.array([
            [float(desiredPoseMsg.data[1])], 
            [float(desiredPoseMsg.data[0])], 
            [float(desiredPoseMsg.data[2])], 
            [float(desiredPoseMsg.data[3])], 
            [float(desiredPoseMsg.data[4])], 
            [float(desiredPoseMsg.data[5])]])

    def controlPose(self, data):
        if (self.firstIteration):       
            #takes the initial pose
            self.initialX = data.pose.pose.position.x
            self.initialY = data.pose.pose.position.y
            self.initialZ = data.pose.pose.position.z
            self.initialqX = data.pose.pose.orientation.x
            self.initialqY = data.pose.pose.orientation.y
            self.initialqZ = data.pose.pose.orientation.z
            self.initialqW = data.pose.pose.orientation.w
            self.firstIteration = False

        else:
            #calculates subsequent poses by subtracting from the initial pose
            xPose = data.pose.pose.position.x - self.initialX
            yPose = data.pose.pose.position.y - self.initialY
            zPose = data.pose.pose.position.z - self.initialZ

            currX = data.pose.pose.orientation.x
            currY = data.pose.pose.orientation.y 
            currZ = data.pose.pose.orientation.z
            currW = data.pose.pose.orientation.w 
            
            q1_inv = [self.initialqX, self.initialqY, self.initialqZ, -self.initialqW] 
            q2 = [currX, currY, currZ, currW]
            qr = quaternion_multiply(q2, q1_inv)
            (rollPose, pitchPose, yawPose) = euler_from_quaternion(qr)
            
            
            
            poseMsg = Float64MultiArray(
                layout=MultiArrayLayout(
                    dim=(MultiArrayDimension(
                        label='dim1',
                        size=6,
                        stride=6
                    ),),
                    data_offset=0
                ),
                data=(-yPose, xPose, zPose, 
                rollPose, pitchPose, yawPose)
            )
            pose = PoseStamped()
            pose.header.frame_id = "map"
            
            pose.pose.position.x = -yPose
            pose.pose.position.y = xPose
            pose.pose.position.z = zPose
            pose.pose.orientation.x = currX
            pose.pose.orientation.y = currY
            pose.pose.orientation.z = currZ
            pose.pose.orientation.w = currW

            
            self.poseStamped.publish(pose)

            self.posePub.publish(poseMsg)
            
        # this part factors in angle wraparound

        differenceRoll = self.desiredPose[3] - rollPose;
        differenceRoll = (differenceRoll + 180) % 360 - 180;

        differencePitch = self.desiredPose[4] - pitchPose;
        differencePitch = (differencePitch + 180) % 360 - 180;

        differenceYaw = self.desiredPose[5] - yawPose;
        differenceYaw = (differenceYaw + 180) % 360 - 180;
        

        xRate = self.xVelPID.update(self.desiredPose[0], xPose)
        yRate = self.yVelPID.update(self.desiredPose[1], yPose)
        zRate = self.zVelPID.update(self.desiredPose[2], zPose)
        rollRate = self.rollVelPID.update(self.desiredPose[3], self.desiredPose[3] - differenceRoll)
        #pitchRate is different to factor in the flipped y and x
        pitchRate = self.pitchVelPID.update(self.desiredPose[4], self.desiredPose[4] + differencePitch)
        yawRate = self.yawVelPID.update(self.desiredPose[5], self.desiredPose[5] - differenceYaw)

        # I had to flip the xRate and yRate here as well
        desiredVelMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(-yRate, xRate, zRate, 
            rollRate, pitchRate, yawRate)
        )

        self.currentPose[0] = -yPose
        self.currentPose[1] = xPose
        self.currentPose[2] = zPose
        self.currentPose[3] = rollPose
        self.currentPose[4] = pitchPose
        self.currentPose[5] = yawPose
        

        self.store_data = np.append(self.store_data, self.currentPose)
        np.save("/home/andrewyuantw/record/simRecord.npy", self.store_data);

        self.VelPub.publish(desiredVelMsg)

        
	
        self.rate.sleep()



if __name__ == '__main__':
    poseControlsNode = poseControlsNode()
    rospy.spin()
