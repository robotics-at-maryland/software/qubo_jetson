#!/usr/bin/env python2
# Required for ROS to execute file as a Python script

'''
@file       velocityTeleopNode.py
@date       2020/10/31
@brief      Qubo teleop node that utilizes the velocity PID controls node
'''


import time
import pygame

import rospy
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension


'''
@brief      Qubo velocity teleoperation node
'''
class VelocityTeleopNode:
    # QUBO FRAME: X=RIGHT, Y=FORWARD, Z=UP

    desiredVelPub = None        # Thruster commands publisher
    rate = None                 # ROS publishing rate object

    xRate = 0                   # Desired linear x rate
    yRate = 0                   # Desired linear y rate
    zRate = 0                   # Desired linear z rate
    rollRate = 0                # Desired roll rate
    pitchRate = 0               # Desired pitch rate
    yawRate = 0                 # Desired yaw rate

    prevKeyStates = None        # Variable to keep track of previous key press states


    '''
    @brief      Class constructor
    @param      None
    @return     None
    '''
    def __init__(self):
        # Initialize ROS node, publishers and subscribers
        rospy.init_node('velocityTeleopNode', anonymous=False)
        self.desiredVelPub = rospy.Publisher('qubo_jetson/desiredVelocity', Float64MultiArray, queue_size=1)

        # Set publishing rate (50 Hz)
        self.rate = rospy.Rate(50)

        # Initialize PyGame
        pygame.init()
        pygame.display.set_mode([100, 100])
        pygame.key.set_repeat(10, 10)

        # Get initial key states
        self.prevKeyStates = pygame.key.get_pressed()


    '''
    @brief      Main application run function
    @param      None
    @return     None
    '''
    def teleop(self):
        # Continue to poll key states while ROS is active
        while (rospy.is_shutdown() == False):
            keyStates = pygame.key.get_pressed()

	        for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    print event.key
                    
            # Increment or decrement Qubo velocities based on key press
            # Only change a velocity when a key transitions not pressed to pressed
            if (keyStates[pygame.K_w] and self.prevKeyStates[pygame.K_w]==False):
		        self.yRate += 1
            elif (keyStates[pygame.K_s] and self.prevKeyStates[pygame.K_s]==False):
                self.yRate -= 1

            elif (keyStates[pygame.K_d] and self.prevKeyStates[pygame.K_d]==False):
                self.xRate += 1
            elif (keyStates[pygame.K_a] and self.prevKeyStates[pygame.K_a]==False):
                self.xRate -= 1

            elif (keyStates[pygame.K_UP] and self.prevKeyStates[pygame.K_UP]==False):
                self.zRate += 1
            elif (keyStates[pygame.K_DOWN] and self.prevKeyStates[pygame.K_DOWN]==False):
                self.zRate -= 1

            elif (keyStates[pygame.K_LEFT] and self.prevKeyStates[pygame.K_LEFT]==False):
                self.yawRate += 1
            elif (keyStates[pygame.K_RIGHT] and self.prevKeyStates[pygame.K_RIGHT]==False):
                self.yawRate -= 1

            elif (keyStates[pygame.K_l] and self.prevKeyStates[pygame.K_l]==False):
                self.rollRate += 1
            elif (keyStates[pygame.K_j] and self.prevKeyStates[pygame.K_j]==False):
                self.rollRate -= 1

            elif (keyStates[pygame.K_k] and self.prevKeyStates[pygame.K_k]==False):
                self.pitchRate += 1
            elif (keyStates[pygame.K_i] and self.prevKeyStates[pygame.K_i]==False):
                self.pitchRate -= 1

            self.prevKeyStates = keyStates
	    

            # Construct very convoluted 6-element 1D vector ROS message
            desiredVelMsg = Float64MultiArray(
                layout=MultiArrayLayout(
                    dim=(MultiArrayDimension(
                        label='dim1',
                        size=6,
                        stride=6
                    ),),
                    data_offset=0
                ),
                data=(self.xRate, self.yRate, self.zRate, 
                self.rollRate, self.pitchRate, self.yawRate)
            )

            # Publish desired velocity message
            print('Desired Velocity Vector: ')
            print(desiredVelMsg.data)
            self.desiredVelPub.publish(desiredVelMsg)
            self.rate.sleep()



if __name__ == '__main__':
    try:
        velocityTeleopNode = VelocityTeleopNode()
        velocityTeleopNode.teleop()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
