#!/usr/bin/env python2

'''
@file       velocityFromIMU.py
@date       2021/1/18
@brief      node with the sole purpose of integrating velocity and pose from IMU data
'''

# Previously, I had done the integration for the Velocity PID and Pose PID separately, but that caused disparities between the values, so now I've moved Kevin's code for integration into this separate node


import numpy as np
import rospy
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Imu
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension
from tf.transformations import euler_from_quaternion
from tf.transformations import quaternion_matrix
from geometry_msgs.msg import PoseStamped


class velocityFromIMU:

    velPub = None
    posePub = None
    linearVelocity = \
    np.array([0.0, 0.0, 0.0])   	# Linear velocity integrated from IMU acceleration
    previousTime = None			# Previous timestamp
    xyzPosition = \
        np.array([0.0, 0.0, 0.0])	# XYZ position integrated from linear velocity
    store_data = np.array([], dtype='f')
    R_sw = None
    Rq_sw = None
    firstPass = True

    def __init__(self):
        rospy.init_node('IMUvelocityNode', anonymous=False)
        self.velPub = rospy.Publisher('qubo_gazebo/velocityFromIntegration', Float64MultiArray, queue_size=1)
        self.posePub = rospy.Publisher('qubo_gazebo/poseFromIntegration', Float64MultiArray, queue_size = 1)
        
        self.counter = 0
        rospy.Subscriber("qubo_gazebo/imu", Imu, self.getVelocity, queue_size=1)
        self.rate = rospy.Rate(30)
        self.poseStamped = rospy.Publisher('qubo_gazebo/imuPoseStamped', PoseStamped, queue_size=1)

    def getVelocity(self, imuMsg):

        # Extract message timestamp
        secs = imuMsg.header.stamp.secs     	# Seconds since epoch
        nsecs = imuMsg.header.stamp.nsecs   	# Nanoseconds since seconds

        currentTime = float(secs) + float(nsecs) / 1000000000

        # Calculate delta time
        if (self.previousTime == None):
            dt = 0
            self.previousTime = currentTime
        else:
            dt = currentTime - self.previousTime
            self.previousTime = currentTime

        # Extract IMU orientation quaternion
        qx = imuMsg.orientation.x
        qy = imuMsg.orientation.y
        qz = imuMsg.orientation.z
        qw = imuMsg.orientation.w

        if (self.firstPass == True):
            # Get rotation matrix from world frame to simulation frame if this is the first time the IMU is read
            self.R_sw = quaternion_matrix([qx, qy, qz, qw])[0:3, 0:3]
            self.Rq_sw = [qx, qy, qz, qw]
            self.firstPass = False

        # Get rotation matrix from quaternion (world frame to Qubo frame)
        R_sq = quaternion_matrix([qx, qy, qz, qw])[0:3, 0:3]
        R_qs = R_sq.T
        R_qw = np.matmul(R_qs, self.R_sw)
        R_wq = R_qw.T

        # Convert gravitational accel vector from world to Qubo frame
        # NOTE: The IMU offset is positive for some reason; need to investigate in the future
        g_s = np.array([[0], [0], [9.8]])
        g_q = np.matmul(R_qs, g_s)
        
        # Extract IMU linear acceleration values and offset by g_q components
        xAccel = imuMsg.linear_acceleration.x - g_q[0][0]
        yAccel = imuMsg.linear_acceleration.y - g_q[1][0]
        zAccel = imuMsg.linear_acceleration.z - g_q[2][0]

        # Integrate accelerate to get velocity

        self.linearVelocity[0] += xAccel * dt
        self.linearVelocity[1] += yAccel * dt
        self.linearVelocity[2] += zAccel * dt

        xRate = self.linearVelocity[0]
        yRate = self.linearVelocity[1]
        zRate = self.linearVelocity[2]

        # Extract angular velocity values
        rollRate = imuMsg.angular_velocity.y
        pitchRate = imuMsg.angular_velocity.x
        yawRate = imuMsg.angular_velocity.z

       

        VelMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(xRate, yRate, zRate, 
            rollRate, pitchRate, yawRate)
        )

        self.velPub.publish(VelMsg)


        linearVelocity_q = np.array([self.linearVelocity[0:3]]).T
        linearVelocity_w = np.matmul(R_wq, linearVelocity_q)

        # Integrate velocity to get position
        self.xyzPosition[0] += linearVelocity_w[0][0] * dt 	
        self.xyzPosition[1] += linearVelocity_w[1][0] * dt	
        self.xyzPosition[2] += linearVelocity_w[2][0] * dt 	

        xPose = self.xyzPosition[0]
        yPose = self.xyzPosition[1]
        zPose = self.xyzPosition[2]

        
        
        
        # No need to integrate for orientation, since the imu provides quaternion values
        q = [qx, qy, qz, qw]
        (rollPose, pitchPose, yawPose) = euler_from_quaternion(q)

        poseMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(xPose, yPose, zPose, 
            rollPose, pitchPose, yawPose)
        )



        pose = PoseStamped()
        pose.header.frame_id = "map"
        
        pose.pose.position.x = xPose
        pose.pose.position.y = yPose
        pose.pose.position.z = zPose
        pose.pose.orientation.x = qx
        pose.pose.orientation.y = qy
        pose.pose.orientation.z = qz
        pose.pose.orientation.w = qw

        self.poseStamped.publish(pose)

        self.store_data = np.append(self.store_data, self.xyzPosition);
        np.save("/home/andrewyuantw/record/imuRecord.npy", self.store_data);

        self.posePub.publish(poseMsg)



if __name__ == '__main__':
    IMUvelocityNode = velocityFromIMU()
    rospy.spin()
