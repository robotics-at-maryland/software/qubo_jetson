#!/usr/bin/env python
# Required for ROS to execute file as a Python script

'''
@file       StateController.py
@date       2021/02/13
@brief      Controller that regulates a 6 element state using a PID controller
'''

# Avoid generating .pyc files
import sys
sys.dont_write_bytecode = True

import numpy as np

from PID import PID


'''
@brief      6 element state controller
'''
class StateController:
    # QUBO FRAME: X=RIGHT, Y=FORWARD, Z=UP

    xPID = None         # X axis linear PID controller
    yPID = None         # Y axis linear PID controller
    zPID = None         # Z axis linear PID controller
    rollPID = None      # Y axis angular PID controller
    pitchPID = None     # X axis angular PID controller
    yawPID = None       # Z axis angular PID controller


    '''
    @brief      Class constructor
    @param      pidMatrix   6x3 NumPy array of coefficients for the PID controllers
    @param      angleFlag   Flag to indicate if the angular PIDs need to be adjusted for wraparound
    @return     None
    '''
    def __init__(
        self, 
        pidMatrix=np.array([
            [0.5, 0.5, 0.9],
            [0.5, 0.5, 0.9],
            [0.5, 0.5, 0.9],
            [0.5, 0.1, 0.6],
            [0.5, 0.1, 0.6],
            [0.5, 0.1, 0.6]]),
        angleFlag=False
        ):
        
        # Initialize a PID object for each state element
        self.xPID = PID(pidMatrix[0][0], pidMatrix[0][1], pidMatrix[0][2])
        self.yPID = PID(pidMatrix[1][0], pidMatrix[1][1], pidMatrix[1][2])
        self.zPID = PID(pidMatrix[2][0], pidMatrix[2][1], pidMatrix[2][2])
        self.rollPID = PID(pidMatrix[3][0], pidMatrix[3][1], pidMatrix[3][2], angleFlag=angleFlag)
        self.pitchPID = PID(pidMatrix[4][0], pidMatrix[4][1], pidMatrix[4][2], angleFlag=angleFlag)
        self.yawPID = PID(pidMatrix[5][0], pidMatrix[5][1], pidMatrix[5][2], angleFlag=angleFlag)


    '''
    @brief      Calculate controller output
    @param      desiredState    6 element NumPy array of desired state
    @param      currentState    6 element NumPy array of current state
    @return     output          6 element NumPy array of controller output
    '''
    def getOutput(self, desiredState, currentState):
        # Compute PID controller outputs
        xOutput = self.xPID.update(desiredState[0], currentState[0])
        yOutput = self.yPID.update(desiredState[1], currentState[1])
        zOutput = self.zPID.update(desiredState[2], currentState[2])
        rollOutput = self.rollPID.update(desiredState[3], currentState[3])
        pitchOutput = self.pitchPID.update(desiredState[4], currentState[4])
        yawOutput = self.yawPID.update(desiredState[5], currentState[5])

        output = np.array([xOutput, yOutput, zOutput, rollOutput, pitchOutput, yawOutput])

        return output



if __name__ == '__main__':
    pass


