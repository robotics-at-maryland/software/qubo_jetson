#!/usr/bin/env python
# Required for ROS to execute file as a Python script

import rospy
import actionlib

from std_msgs.msg import String
from geometry_msgs.msg import Wrench
from geometry_msgs.msg import Vector3
from nav_msgs.msg import Odometry

from tf.transformations import euler_from_quaternion
import tf
import tf2_geometry_msgs
from geometry_msgs.msg import TransformStamped, Vector3Stamped
from tf.transformations import quaternion_matrix

import numpy as np

import qubo_jetson.msg


class ControlsNode:
    pub = None
    rate = None

    desiredPose = None

    K = np.array([
        [1,0,0,0,0,0],
        [0,1,0,0,0,0],
        [0,0,1,0,0,0],
        [0,0,0,1,0,0],
        [0,0,0,0,10,0],
        [0,0,0,0,0,1]])


    def __init__(self):
        rospy.init_node('controlsNode', anonymous=True)
        self.pub = rospy.Publisher('qubo_gazebo/thruster_manager/input', Wrench, queue_size=1)
        rospy.Subscriber("qubo_jetson/desiredPose", String, self.desiredPoseCB, queue_size = 1)
        rospy.Subscriber("qubo_gazebo/pose_gt", Odometry, self.run, queue_size = 1)
        self.rate = rospy.Rate(10)   # 10hz

        #self.changePoseAS = actionlib.SimpleActionServer('changePoseAS', changePoseAction, self.changePose, auto_start=False)
        #self.changePoseAS.start()


    def desiredPoseCB(self, desiredPoseMsg):
        self.desiredPose = np.array([
            [float(desiredPoseMsg[0])], 
            [float(desiredPoseMsg[1])], 
            [float(desiredPoseMsg[2])], 
            [float(desiredPoseMsg[3])], 
            [float(desiredPoseMsg[4])], 
            [float(desiredPoseMsg[5])]])


    def run(self, poseMsg):
        if (self.desiredPose == None):
            return

        x = poseMsg.pose.pose.position.x
        y = poseMsg.pose.pose.position.y
        z = poseMsg.pose.pose.position.z

        qx = poseMsg.pose.pose.orientation.x
        qy = poseMsg.pose.pose.orientation.y
        qz = poseMsg.pose.pose.orientation.z
        qw = poseMsg.pose.pose.orientation.w

        eulerOrientation = euler_from_quaternion((qx, qy, qz, qw))
        roll = eulerOrientation[0]
        pitch = eulerOrientation[1]
        yaw = eulerOrientation[2]

        q = (qx, qy, qz, qw)
        #q = tf.transformations.quaternion_inverse((qx, qy, qz, qw))

        T = quaternion_matrix([q[0], q[1], q[2], q[3]])
        motionVectorW = np.array([
            [self.desiredPose[0,0] - x],
            [self.desiredPose[1,0] - y],
            [self.desiredPose[2,0] - z]])
        
        motionVectorQ = np.matmul(np.linalg.inv(T[0:3,0:3]), motionVectorW)

        orientationError = np.array([
            [self.desiredPose[3,0] - roll],
            [self.desiredPose[4,0] - pitch],
            [self.desiredPose[5,0] - yaw]])


        stateError = np.concatenate((motionVectorQ, orientationError), axis=0)
        thrusterInput = np.matmul(self.K, stateError)

        thrusterInputMsg = \
            Wrench(
                force=Vector3(x=thrusterInput[0], y=thrusterInput[1], z=thrusterInput[2]), 
                torque=Vector3(x=thrusterInput[4], y=thrusterInput[3], z=thrusterInput[5]))
        
        print('x: ' + str(x))
        print('y: ' + str(y))
        print('z: ' + str(z))
        print('motionVector: ' + str(motionVectorQ) + '\n')
        print('matrix: ' + str(T[0:3,0:3]) + '\n')
        #print('thruster: ' + str(thrusterInput) + '\n')

        #rospy.loginfo(thrusterInputMsg)
        self.pub.publish(thrusterInputMsg)
        self.rate.sleep()



if __name__ == '__main__':
    controlsNode = ControlsNode()
    rospy.spin()

    '''
    try:
        controlsNode.run()
    except rospy.ROSInterruptException:
        pass
    '''