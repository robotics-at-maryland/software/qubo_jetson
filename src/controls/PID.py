'''
@file       PID.py
@date       2020/10/31
@brief      Simple 1D PID controller
'''

import math
from collections import deque

'''
@brief      Simple 1D PID controller
'''
class PID:
    P = 0                       # Proportional gain
    I = 0                       # Integral gain
    D = 0                       # Derivative gain

    Imax = None                 # Upper limit on integrated error
    Imin = None                 # Lower limit on integrated error

    integratedError = 0         # Integrated (accumulated) error
    previousError = None        # Error from previous time step

    setpoint = 0                # Desired value to reach
    
    deltaErrorQ = None          # Queue of deltaError values
    deltaQueueSize = 0          # Length of deltaErrorQ

    angleFlag = False           # Flag to indicate if the PID is used with angle values


    '''
    @brief      Class constructor
    @param      See above for descriptions
    @return     None
    '''
    def __init__(self, P, I, D, Imax=5, Imin=-5, deltaQueueSize=5, angleFlag=False):
        # Set controller parameters
        self.P = P
        self.I = I
        self.D = D
        self.Imax = Imax
        self.Imin = Imin

        self.deltaErrorQ = deque([0] * deltaQueueSize)
        self.deltaQueueSize = deltaQueueSize

        self.angleFlag = angleFlag


    '''
    @brief      Updates PID state and generates controller output
    @param      setpoint        Desired value to reach
    @param      currentValue    Current system value
    @return     output          Signed PID controller output response value
    '''
    def update(self, setpoint, currentValue):
        # If a new setpoint is requested, reset the integral and derivative histories
        if (setpoint != self.setpoint):
            self.integratedError = 0
            self.previousError = None
            self.setpoint = setpoint

        # Compute the error
        currentError = setpoint - currentValue

        # Correct for angle wraparound if the PID is used with angle values
        if (self.angleFlag == True):
            if (currentError > math.pi):
                currentError -= 2*math.pi
                
            elif (currentError < -math.pi):
                currentError += 2*math.pi

        # Update and constrain the integrated error
        self.integratedError += currentError
        self.integratedError = max(min(self.integratedError, self.Imax), self.Imin)

        # Compute the error derivative and update queue
        if (self.previousError == None):
            deltaError = 0
            self.deltaErrorQ.append(deltaError)
            self.deltaErrorQ.popleft()
            self.previousError = currentError
        else:
            deltaError = currentError - self.previousError
            self.deltaErrorQ.append(deltaError)
            self.deltaErrorQ.popleft()
            self.previousError = currentError
        
        # Compute the signed controller output
        meanDeltaError = sum(self.deltaErrorQ) / self.deltaQueueSize
        output = self.P*currentError + self.I*self.integratedError + self.D*meanDeltaError

        return output
