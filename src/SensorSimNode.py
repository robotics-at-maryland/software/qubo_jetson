#!/usr/bin/env python
# Required for ROS to execute file as a Python script

'''
@file       SensorSimNode.py
@date       2021/06/05
@brief      Simulation node that modifies Gazebo sensor topics to match those in real life
'''

# Avoid generating .pyc files
import sys
sys.dont_write_bytecode = True

import rospy
from geometry_msgs.msg import TwistWithCovarianceStamped
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry

import math
import numpy as np

from tf.transformations import euler_from_quaternion
from tf.transformations import quaternion_from_euler
from tf.transformations import quaternion_matrix
from tf.transformations import euler_from_quaternion
from tf.transformations import quaternion_inverse
from tf.transformations import quaternion_multiply


'''
@brief      Simulation node that modifies Gazebo sensor topics to match those in real life
'''
class SensorSimNode:
    # Quaternion of R_sw from world frame to simulation frame (x,y,z,w)
    Rq_sw = None

    # DVL linear velocity error specs
    # Page 36, Section 9.1 Program Specifications
    # Bottom Track Phased Array Accuracy: 
    # +/- 0.4% above 0.5 m/s
    # +/- 0.2 cm/s below 0.5 m/s
    # NOTE: Need to confirm
    dvlVelMean = 0
    dvlVelStdev = 0.005

    # IMU orientation error specs
    # Page 3, Program specifications, accuracy is 2 degrees rms
    imuOrientationMean = 0
    imuOrientationStdev = 2

    # Modified messages for new sensor topic outputs
    newImuMsg = Imu()
    newDvlMsg = TwistWithCovarianceStamped()


    '''
    @brief      Class constructor
    @param      None
    @return     None
    '''
    def __init__(self):
        # Initialize ROS node
        rospy.init_node('noiseNode', anonymous=False)

        # Initialize subscribers for Gazebo topics to modify
        rospy.Subscriber('qubo_gazebo/pose_gt', Odometry, self.createNoise, queue_size = 1)
        rospy.Subscriber("qubo_gazebo/imu", Imu, self.republishIMU, queue_size=1)

        # Initialize publishers for modified sensor topics
        self.IMUPub = rospy.Publisher('qubo_gazebo/IMUwithNoise', Imu, queue_size = 1)
        self.DVLPub = rospy.Publisher('qubo_gazebo/DVLwithNoise', TwistWithCovarianceStamped, queue_size = 1)

        self.firstIteration = True
        self.rate = rospy.Rate(30)


    '''
    @brief      Inject noise into ground truth linear velocity and orientation
    @param      gtPoseMsg       Ground truth pose message
    @return     None
    '''
    def createNoise(self, gtPoseMsg):
        # Get rotation matrix and quaternion from world frame to simulation frame if this is the first iteration
        if (self.firstIteration):       
            # Takes the initial orientation as a quaternion
            initialqX = gtPoseMsg.pose.pose.orientation.x
            initialqY = gtPoseMsg.pose.pose.orientation.y
            initialqZ = gtPoseMsg.pose.pose.orientation.z
            initialqW = gtPoseMsg.pose.pose.orientation.w

            self.firstIteration = False

            self.Rq_sw = [initialqX, initialqY, initialqZ, initialqW]

        # Calculate subsequent poses by applying the initial pose
        currqX = gtPoseMsg.pose.pose.orientation.x
        currqY = gtPoseMsg.pose.pose.orientation.y 
        currqZ = gtPoseMsg.pose.pose.orientation.z
        currqW = gtPoseMsg.pose.pose.orientation.w 
        
        # Convert GT orientation quaternion to represent rotation from Qubo frame to  world frame
        Rq_sq = [currqX, currqY, currqZ, currqW]
        Rq_qs = quaternion_inverse(Rq_sq)
        Rq_qw = quaternion_multiply(Rq_qs, self.Rq_sw)
        Rq_wq = quaternion_inverse(Rq_qw)

        # Convert IMU quaternion to angular pose
        (pitchPose, rollPose, yawPose) = euler_from_quaternion(Rq_wq)

        '''
        q1_inv = [self.initialqX, self.initialqY, self.initialqZ, -self.initialqW] 
        q2 = [currqX, currqY, currqZ, currqW]
        qr = quaternion_multiply(q2, q1_inv)
        (rollPose, pitchPose, yawPose) = euler_from_quaternion(qr)
        '''

        # Add orientation noise according to IMU sensor specifications
        gtOrientation = np.array([rollPose, pitchPose, yawPose])
        orientationNoise = np.random.normal(
            math.radians(self.imuOrientationMean), 
            math.radians(self.imuOrientationStdev), 
            3)
        noisyOrientation = gtOrientation + orientationNoise

        # Transforms Euler angles back to quaterion
        quaternion = quaternion_from_euler(noisyOrientation[0], noisyOrientation[1], noisyOrientation[2])
        
        # Update and publish new IMU message
        self.newImuMsg.header = gtPoseMsg.header
        self.newImuMsg.orientation.x = quaternion[0]
        self.newImuMsg.orientation.y = quaternion[1]
        self.newImuMsg.orientation.z = quaternion[2]
        self.newImuMsg.orientation.w = quaternion[3]

        self.IMUPub.publish(self.newImuMsg)

        # Get GT linear velocity in simulation frame
        xVel = gtPoseMsg.twist.twist.linear.x
        yVel = gtPoseMsg.twist.twist.linear.y 
        zVel = gtPoseMsg.twist.twist.linear.z

        # Transform linear velocity from simulation frame to Qubo frame
        R_qs = quaternion_matrix(Rq_qs)[0:3, 0:3]
        gtLinearVel_s = np.array([[xVel], [yVel], [zVel]])
        gtLinearVel_q = np.matmul(R_qs, gtLinearVel_s)

        # Add velocity noise according to DVL sensor specifications
        velNoise = np.random.normal(self.dvlVelMean, self.dvlVelStdev, 3)
        simLinearVel = gtLinearVel_q.flatten() + velNoise

        # Update and publish new DVL message
        self.newDvlMsg.header = gtPoseMsg.header
        self.newDvlMsg.twist.twist.linear.x = simLinearVel[0]
        self.newDvlMsg.twist.twist.linear.y = simLinearVel[1]
        self.newDvlMsg.twist.twist.linear.z = simLinearVel[2]

        self.DVLPub.publish(self.newDvlMsg)


    '''
    @brief      Update IMU linear acceleration and angular velocity and republish
    @param      imuMsg          IMU message
    @return     None
    '''
    def republishIMU(self, imuMsg):
        self.newImuMsg.header = imuMsg.header

        # Extract and update acceleration values
        self.newImuMsg.linear_acceleration.x = imuMsg.linear_acceleration.x
        self.newImuMsg.linear_acceleration.y = imuMsg.linear_acceleration.y
        self.newImuMsg.linear_acceleration.z = imuMsg.linear_acceleration.z

        # Extract and update angular velocity values
        self.newImuMsg.angular_velocity.x = imuMsg.angular_velocity.x
        self.newImuMsg.angular_velocity.y = imuMsg.angular_velocity.y
        self.newImuMsg.angular_velocity.z = imuMsg.angular_velocity.z

        # Publishes new IMU message
        self.IMUPub.publish(self.newImuMsg)



if __name__ == '__main__':
    sensorSimNode = SensorSimNode()
    rospy.spin()
    