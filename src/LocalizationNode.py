#!/usr/bin/env python
# Required for ROS to execute file as a Python script

'''
@file       LocalizationNode.py
@date       2021/02/27
@brief      Qubo controls node that stabilizes 6 DOF velocity using a PID controller
'''

# Avoid generating .pyc files
import sys
sys.dont_write_bytecode = True

import rospy
from sensor_msgs.msg import Imu
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import TwistWithCovarianceStamped
from tf.transformations import quaternion_from_matrix, quaternion_multiply, quaternion_inverse, euler_from_quaternion, quaternion_from_euler

import numpy as np

from localization.IMULocalizer import IMULocalizer
from localization.DVLLocalizer import DVLLocalizer
from localization.KFLocalizer import KFLocalizer

'''
@brief      Qubo controls ROS node
'''
class LocalizationNode:
    # QUBO FRAME: X=RIGHT, Y=FORWARD, Z=UP
    # Pose is expressed in world frame
    # Velocity is expressed in Qubo frame
    
    velocityPub = None                  # Current velocity publisher (IMU)
    posePub = None                      # Current pose publisher (IMU)
    gatePosPub = None                   # Gate Position Publisher
    dvlVelocityPub = None               # Current velocity publisher (DVL)
    dvlPosePub = None                   # Current pose publisher (DVL)
    rotationMatrixPub = None            # R_qw publisher
    stateCovMatrixPub = None            # State covariance publisher (KF)

    imuPoseStamped = None               # Current pose publisher for Rviz (IMU)
    dvlPoseStamped = None               # Current pose publisher for Rviz (DVL)

    rate = None                         # ROS publishing rate object

    imuPreviousTime = None              # Previous time stamp
    dvlPreviousTime = None

    imuMsg = None
    dvlMsg = None

    # Rotation matrix from world frame to Qubo frame
    # Used in pose control
    R_qw = np.zeros((3,3))
    currentPosition = [0.0, 0.0, 0.0]

    # Localizer objects
    imuLocalizer = IMULocalizer()
    dvlLocalizer = DVLLocalizer()
    kfLocalizer = KFLocalizer()     

    

    # Max number of past positions to track
    maxPastGatePositions = 10
    currentGatePositionArrayNum = 0

    # NumPy array of past n global gate position
    pastGatePositions = np.empty((maxPastGatePositions,3), dtype=np.float64)
    

    # Max number of past positions to track
    maxPastGateOrientations =10
    currentGateOrientationArrayNum = 0
   
    # NumPy array of past n global gate orientations
    pastGateOrientations = np.empty((maxPastGateOrientations,3), dtype=np.float64)


    '''
    @brief      Class constructor
    @param      None
    @return     None
    '''
    def __init__(self, use_ground_truth=False):
        self.use_ground_truth = use_ground_truth

        # Initialize ROS node
        rospy.init_node('LocalizationNode', anonymous=False)

        # Initialize velocity and pose publishers
        self.velocityPub = rospy.Publisher('qubo_jetson/currentVelocity', Float64MultiArray, queue_size=1)
        self.posePub = rospy.Publisher('qubo_jetson/currentPose', Float64MultiArray, queue_size=1)
        self.gatePosPub = rospy.Publisher('qubo_jetson/worldGatePose', Float64MultiArray, queue_size=1)
        self.rotationMatrixPub = rospy.Publisher('qubo_jetson/R_qw', Float64MultiArray, queue_size=1)
        self.stateCovMatrixPub = rospy.Publisher('qubo_jetson/stateCovMatrix', Float64MultiArray, queue_size=1)

        self.estimatedPosePub = rospy.Publisher("qubo_gazebo/estimatedPoseStamped", PoseStamped, queue_size=1)
        self.estimatedVelPub = rospy.Publisher("qubo_gazebo/estimatedVelStamped", PoseStamped, queue_size=1)

        # Set publishing rate
        self.rate = rospy.Rate(30)

        # Get control mode from ROS node input
        localizerMode = rospy.get_param('localizerMode')

        rospy.Subscriber("qubo_jetson/R_qw", Float64MultiArray, self.rotationMatrixUpdate, queue_size=1)
        rospy.Subscriber("qubo_jetson/currentPose", Float64MultiArray, self.currentPositionUpdate, queue_size=1)

        if self.use_ground_truth:
            rospy.Subscriber("qubo_gazebo/gate_pose_gt", Float64MultiArray, self.gatePosPub.publish, queue_size=1)
        else:
            rospy.Subscriber("qubo_jetson/gatePose", Float64MultiArray, self.convertGatePose, queue_size=1)

        # Initialize different sets of subscribers based on control mode input
        if (localizerMode == 'IMU'):
            rospy.Subscriber("qubo_gazebo/IMUwithNoise", Imu, self.localizeIMU, queue_size=1)

        elif (localizerMode == 'DVL'):
            rospy.Subscriber("qubo_gazebo/DVLwithNoise", TwistWithCovarianceStamped, self.localizeDVL, queue_size=1)
            rospy.Subscriber("qubo_gazebo/IMUwithNoise", Imu, self.localizeDVLOrientation, queue_size=1)

        elif (localizerMode == 'KF'):
            rospy.Subscriber("qubo_gazebo/DVLwithNoise", TwistWithCovarianceStamped, self.localizeKF, queue_size=1)
            rospy.Subscriber("qubo_gazebo/IMUwithNoise", Imu, self.imuMsgUpdate, queue_size=1)

        else:
            # Shut down node if an invalid control mode is provided
            rospy.signal_shutdown('Invalid localizer mode')
        

    '''
    @brief      Callback function to update the IMU ROS message
    @param      imuMsg      IMU ROS message
    @return     None
    '''
    def imuMsgUpdate(self, imuMsg):
        self.imuMsg = imuMsg


    '''
    @brief      Callback function to localize using just IMU
    @param      imuMsg      IMU ROS message
    @return     None
    '''
    def localizeIMU(self, imuMsg):
        # Extract message timestamp
        secs = imuMsg.header.stamp.secs     # Seconds since epoch
        nsecs = imuMsg.header.stamp.nsecs    # Nanoseconds since seconds

        currentTime = float(secs) + float(nsecs) / 1000000000

        # Calculate delta time
        if (self.imuPreviousTime == None):
            dt = 0
            self.imuPreviousTime = currentTime
        else:
            dt = currentTime - self.imuPreviousTime
            self.imuPreviousTime = currentTime

        # Get current velocity and pose estimate from the IMU localizer
        velocity, pose, R_qw = self.imuLocalizer.estimateState(imuMsg, dt)

        #print('Velocity:')
        #print(np.round(velocity, decimals=1).tolist())
        #print('Pose:')
        #print(np.round(pose, decimals=1).tolist())
        #print('')

        # Construct very convoluted 6-element 1D vector ROS message
        # for current pose and velocity vectors
        currentVelocityMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(velocity[0], velocity[1], velocity[2], 
                velocity[3], velocity[4], velocity[5])
        )

        currentPoseMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(pose[0], pose[1], pose[2], 
                pose[3], pose[4], pose[5])
        )

        R_qwMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=([
                MultiArrayDimension(
                    label='row',
                    size=3,
                    stride=3*3
                ),
                MultiArrayDimension(
                    label='col',
                    size=3,
                    stride=3
                )]),
                data_offset=0
            ),
            data=R_qw.reshape(R_qw.size).tolist()
        )

        # Publish desired velocity message
        self.velocityPub.publish(currentVelocityMsg)
        self.posePub.publish(currentPoseMsg)
        self.rotationMatrixPub.publish(R_qwMsg)

        # Constructs a PoseStamped message to be used by Rviz and the logging node
        poseStamped = PoseStamped()
        poseStamped.header = imuMsg.header
        poseStamped.header.frame_id = "map"
        poseStamped.pose.position.x = pose[0]
        poseStamped.pose.position.y = pose[1]
        poseStamped.pose.position.z = pose[2]
        poseStamped.pose.orientation.x = imuMsg.orientation.x
        poseStamped.pose.orientation.y = imuMsg.orientation.y
        poseStamped.pose.orientation.z = imuMsg.orientation.z
        poseStamped.pose.orientation.w = imuMsg.orientation.w

        self.estimatedPosePub.publish(poseStamped)

        # Constructs another "PoseStamped" message for velocity to be used by the logging node
        # I am doing this to get the time stamps since time stamps need to be logged
        poseStamped.pose.position.x = velocity[0]
        poseStamped.pose.position.y = velocity[1]
        poseStamped.pose.position.z = velocity[2]

        self.estimatedVelPub.publish(poseStamped)

        self.rate.sleep()


    '''
    @brief      Callback function to localize using IMU and DVL
    @param      None
    @return     None
    '''
    def localizeDVL(self, dvlMsg):
        # Extract message timestamp
        secs = dvlMsg.header.stamp.secs         # Seconds since epoch
        nsecs = dvlMsg.header.stamp.nsecs       # Nanoseconds since seconds

        currentTime = float(secs) + float(nsecs) / 1000000000

        # Calculate delta time
        if (self.dvlPreviousTime == None):
            dt = 0
            self.dvlPreviousTime = currentTime
        else:
            dt = currentTime - self.dvlPreviousTime
            self.dvlPreviousTime = currentTime

        self.dvlMsg = dvlMsg

        velocity, pose, R_qw = self.dvlLocalizer.estimateState(dvlMsg, self.imuMsg, dt)

        #print('Velocity:')
        #print(np.round(velocity, decimals=1).tolist())
        #print('Pose:')
        #print(np.round(pose, decimals=1).tolist())
        #print('')

        currentVelocityMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(velocity[0], velocity[1], velocity[2], 
                velocity[3], velocity[4], velocity[5])
        )

        currentPoseMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(pose[0], pose[1], pose[2], 
                pose[3], pose[4], pose[5])
        )

        R_qwMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=([
                MultiArrayDimension(
                    label='row',
                    size=3,
                    stride=3*3
                ),
                MultiArrayDimension(
                    label='col',
                    size=3,
                    stride=3
                )]),
                data_offset=0
            ),
            data=R_qw.reshape(R_qw.size).tolist()
        )

        # Publish desired velocity message
        self.velocityPub.publish(currentVelocityMsg)
        self.posePub.publish(currentPoseMsg)
        self.rotationMatrixPub.publish(R_qwMsg)

        # Constructs a pose stamped message to be used by Rviz and the logging node
        poseStamped = PoseStamped()
        poseStamped.header = dvlMsg.header
        poseStamped.header.frame_id = "map"
        poseStamped.pose.position.x = pose[0]
        poseStamped.pose.position.y = pose[1]
        poseStamped.pose.position.z = pose[2]

        poseStamped.pose.orientation.x = self.imuMsg.orientation.x
        poseStamped.pose.orientation.y = self.imuMsg.orientation.y
        poseStamped.pose.orientation.z = self.imuMsg.orientation.z
        poseStamped.pose.orientation.w = self.imuMsg.orientation.w

        self.estimatedPosePub.publish(poseStamped)

        # Constructs another "PoseStamped" message for velocity to be used by the logging node
        # I am doing this to get the time stamps since time stamps need to be logged
        poseStamped.pose.position.x = velocity[0]
        poseStamped.pose.position.y = velocity[1]
        poseStamped.pose.position.z = velocity[2]

        self.estimatedVelPub.publish(poseStamped)

        self.rate.sleep()

    '''
    @brief      
    '''
    def localizeDVLOrientation(self, imuMsg):
    
        # Extract message timestamp
        secs = imuMsg.header.stamp.secs         # Seconds since epoch
        nsecs = imuMsg.header.stamp.nsecs       # Nanoseconds since seconds

        currentTime = float(secs) + float(nsecs) / 1000000000

        # Calculate delta time
        if (self.dvlPreviousTime == None):
            dt = 0
            self.dvlPreviousTime = currentTime
        else:
            dt = currentTime - self.dvlPreviousTime
            self.dvlPreviousTime = currentTime

        self.imuMsg = imuMsg;

        velocity, pose, R_qw = self.dvlLocalizer.estimateState(self.dvlMsg, imuMsg, dt)

        #print('Velocity:')
        #print(np.round(velocity, decimals=1).tolist())
        #print('Pose:')
        #print(np.round(pose, decimals=1).tolist())
        #print('')

        currentVelocityMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(velocity[0], velocity[1], velocity[2], 
                velocity[3], velocity[4], velocity[5])
        )

        currentPoseMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(pose[0], pose[1], pose[2], 
                pose[3], pose[4], pose[5])
        )

        R_qwMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=([
                MultiArrayDimension(
                    label='row',
                    size=3,
                    stride=3*3
                ),
                MultiArrayDimension(
                    label='col',
                    size=3,
                    stride=3
                )]),
                data_offset=0
            ),
            data=R_qw.reshape(R_qw.size).tolist()
        )

        # Publish desired velocity message
        self.velocityPub.publish(currentVelocityMsg)
        self.posePub.publish(currentPoseMsg)
        self.rotationMatrixPub.publish(R_qwMsg)

        # Constructs a pose stamped message to be used by Rviz and the logging node
        poseStamped = PoseStamped()
        poseStamped.header = imuMsg.header
        poseStamped.header.frame_id = "map"
        poseStamped.pose.position.x = pose[0]
        poseStamped.pose.position.y = pose[1]
        poseStamped.pose.position.z = pose[2]

        poseStamped.pose.orientation.x = imuMsg.orientation.x
        poseStamped.pose.orientation.y = imuMsg.orientation.y
        poseStamped.pose.orientation.z = imuMsg.orientation.z
        poseStamped.pose.orientation.w = imuMsg.orientation.w

        self.estimatedPosePub.publish(poseStamped)

        # Constructs another "PoseStamped" message for velocity to be used by the logging node
        # I am doing this to get the time stamps since time stamps need to be logged
        poseStamped.pose.position.x = velocity[0]
        poseStamped.pose.position.y = velocity[1]
        poseStamped.pose.position.z = velocity[2]

        self.estimatedVelPub.publish(poseStamped)

        self.rate.sleep()

    
    '''
    @brief      Callback function to localize using Kalman Filter
    @param      None
    @return     None
    '''
    def localizeKF(self, dvlMsg):
        # Extract message timestamp
        secs = dvlMsg.header.stamp.secs     # Seconds since epoch
        nsecs = dvlMsg.header.stamp.nsecs    # Nanoseconds since seconds

        currentTime = float(secs) + float(nsecs) / 1000000000

        # Calculate delta time
        if (self.dvlPreviousTime == None):
            dt = 0
            self.dvlPreviousTime = currentTime
        else:
            dt = currentTime - self.dvlPreviousTime
            self.dvlPreviousTime = currentTime

        # Get current velocity, pose, R_qw and state covariance matrix from the KF localizer
        velocity, pose, R_qw, P = self.kfLocalizer.estimateState(self.imuMsg, dvlMsg, dt)

        #print('Velocity:')
        #print(np.round(velocity, decimals=1).tolist())
        #print('Pose:')
        #print(np.round(pose, decimals=1).tolist())
        #print('')

        # Construct very convoluted 6-element 1D vector ROS message
        # for current pose and velocity vectors
        currentVelocityMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(velocity[0], velocity[1], velocity[2], 
                velocity[3], velocity[4], velocity[5])
        )

        currentPoseMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(pose[0], pose[1], pose[2], 
                pose[3], pose[4], pose[5])
        )

        R_qwMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=([
                MultiArrayDimension(
                    label='row',
                    size=3,
                    stride=3*3
                ),
                MultiArrayDimension(
                    label='col',
                    size=3,
                    stride=3
                )]),
                data_offset=0
            ),
            data=R_qw.reshape(R_qw.size).tolist()
        )

        stateCovMatrixMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=([
                MultiArrayDimension(
                    label='row',
                    size=6,
                    stride=6*6
                ),
                MultiArrayDimension(
                    label='col',
                    size=6,
                    stride=6
                )]),
                data_offset=0
            ),
            data=P.reshape(P.size).tolist()
        )

        # Publish desired velocity message
        self.velocityPub.publish(currentVelocityMsg)
        self.posePub.publish(currentPoseMsg)
        self.rotationMatrixPub.publish(R_qwMsg)
        self.stateCovMatrixPub.publish(stateCovMatrixMsg)

        # Publish pose in a PoseStamped imuMsgmessage so Rviz can visualize the pose
        poseStamped = PoseStamped()
        poseStamped.header = dvlMsg.header
        poseStamped.header.frame_id = "map"
        poseStamped.pose.position.x = pose[0]
        poseStamped.pose.position.y = pose[1]
        poseStamped.pose.position.z = pose[2]
        poseStamped.pose.orientation.x = self.imuMsg.orientation.x
        poseStamped.pose.orientation.y = self.imuMsg.orientation.y
        poseStamped.pose.orientation.z = self.imuMsg.orientation.z
        poseStamped.pose.orientation.w = self.imuMsg.orientation.w

        self.estimatedPosePub.publish(poseStamped)

        # Constructs another "PoseStamped" message for velocity to be used by the logging node
        # I am doing this to get the time stamps since time stamps need to be logged
        poseStamped.pose.position.x = velocity[0]
        poseStamped.pose.position.y = velocity[1]
        poseStamped.pose.position.z = velocity[2]

        self.estimatedVelPub.publish(poseStamped)

        self.rate.sleep()

    '''
    @brief      Callback function to update the R_qw matrix
    @param      desiredPoseMsg     ROS message with desired velocity values
    @return     None
    '''
    def rotationMatrixUpdate(self, R_qwMsg):
        dim0 = R_qwMsg.layout.dim[0]
        dim1 = R_qwMsg.layout.dim[1]

        for i in range(dim0.size):
            for j in range(dim1.size):
                self.R_qw[i][j] = float(R_qwMsg.data[dim1.stride*i + j])

    def currentPositionUpdate(self, currentPositionUpdateMsg):
        currentPosition = currentPositionUpdateMsg

    """
    @brief  Callbck function to continually update gate pose
    @param  gatePoseMsg     ROS Message with current relative gate pose
    @return None
    """
    def convertGatePose(self, gatePoseMsg):
        # Extract current gate pose vector values into an array
        currentGatePose = np.array([
            [float(gatePoseMsg.data[0])], 
            [float(gatePoseMsg.data[1])], 
            [float(gatePoseMsg.data[2])], 
            [float(gatePoseMsg.data[3])], 
            [float(gatePoseMsg.data[4])], 
            [float(gatePoseMsg.data[5])]])

        currentGatePos_q = currentGatePose[0:3]
        rel_trans = np.matmul(self.R_qw.T, currentGatePos_q)
        print("Rel Trans: ", rel_trans)
        currentGatePos_w = np.add(rel_trans[:,0],np.array(self.currentPosition).T) 

        print(currentGatePos_w)

        rotationMatrix = np.array([
            [self.R_qw[0,0], self.R_qw[0,1], self.R_qw[0,2], 0],
            [self.R_qw[1,0], self.R_qw[1,1], self.R_qw[1,2], 0],
            [self.R_qw[2,0], self.R_qw[2,1], self.R_qw[2,2], 0],
            [0, 0, 0, 1]
        ])
    
        rotationMatrixQuaternion = quaternion_from_matrix(rotationMatrix)
        rotationMatrixQuaternionInverse = quaternion_inverse(rotationMatrixQuaternion)
        currentGateOrientation_q = quaternion_from_euler(currentGatePose[3], currentGatePose[4], currentGatePose[5])

        currentGateOrientation_w = euler_from_quaternion(quaternion_multiply(quaternion_multiply(rotationMatrixQuaternion,currentGateOrientation_q),rotationMatrixQuaternionInverse))
        
        self.currentGatePositionArrayNum += 1
        print(self.currentGatePositionArrayNum % self.maxPastGatePositions)
        self.pastGatePositions[self.currentGatePositionArrayNum % self.maxPastGatePositions, :] = currentGatePos_w[:]
        
        if self.currentGatePositionArrayNum >= self.maxPastGatePositions:
            medianGatePosition = np.median(self.pastGatePositions, axis=0)
        else:
            medianGatePosition = np.median(self.pastGatePositions[:self.currentGatePositionArrayNum + 1], axis=0)

        self.currentGateOrientationArrayNum = (self.currentGateOrientationArrayNum + 1) 
        self.pastGateOrientations[self.currentGateOrientationArrayNum% self.maxPastGateOrientations] = currentGateOrientation_w

        if self.currentGateOrientationArrayNum >= self.maxPastGateOrientations:
            medianGateOrientation = np.median(self.pastGateOrientations, axis=0)
        else:
            medianGateOrientation = np.median(self.pastGateOrientations[:self.currentGateOrientationArrayNum + 1], axis=0)
        print(self.pastGatePositions)
        print("Median Gate Position:", medianGatePosition)
        currentGatePoseMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(
                medianGatePosition[0],
                medianGatePosition[1],
                medianGatePosition[2],
                medianGateOrientation[0],
                medianGateOrientation[1],
                medianGateOrientation[2]
            )
        )
        self.gatePosPub.publish(currentGatePoseMsg)
        self.rate.sleep()


if __name__ == '__main__':
    localizationNode = LocalizationNode(use_ground_truth=True)
    rospy.spin()
