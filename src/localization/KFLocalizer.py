#!/usr/bin/env python
# Required for ROS to execute file as a Python script

'''
@file       KFLocalizer.py
@date       2021/02/27
@brief      State estimator that uses a Kalman Filter with IMU and DVL measurements
'''

# Avoid generating .pyc files
import sys
sys.dont_write_bytecode = True

from tf.transformations import quaternion_matrix
from tf.transformations import euler_from_quaternion
from tf.transformations import quaternion_inverse
from tf.transformations import quaternion_multiply

import numpy as np


'''
@brief      Qubo Kalman Filter state estimator
'''
class KFLocalizer:
    # QUBO FRAME: X=RIGHT, Y=FORWARD, Z=UP
    # Pose is expressed in world frame
    # Velocity is expressed in Qubo frame

    # Rotation matrix from world frame to simulation frame
    # Simulation Frame: Hardcoded frame at the center of the Gazebo pool
    # World Frame: Qubo's frame taken at the beginning of a run
    # NOTE: This matrix should not be needed. Consider changing the IMU output topic.
    R_sw = None

    # R_sw in quaternion form as a list (x,y,z,w)
    Rq_sw = None

    # Gravity offset vector in world frame
    g_w = np.array([[0.0], [0.0], [0.0]])

    # Flag that indicates if R_sw is already initialized
    firstPass = True

    # System linear state column vector (position and velocity)
    x = np.zeros((6,1))

    # System state covariance matrix
    P = np.zeros((6,6))

    # Acceleration input covariance matrix
    Q_a = np.array([
        [0.1, 0.0, 0.0],
        [0.0, 0.1, 0.0],
        [0.0, 0.0, 0.1]
    ])

    # Measurement moise covariance matrix
    R = np.array([
        [0.005, 0.0, 0.0],
        [0.0, 0.005, 0.0],
        [0.0, 0.0, 0.005]
    ])

    # 6 element current velocity vector (Qubo frame)
    # Order: x,y,z,roll,pitch,yaw
    velocity = \
        np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

    # 6 element current pose vector (world frame)
    # Order: x,y,z,roll,pitch,yaw
    pose = \
        np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])


    '''
    @brief      Class constructor
    @param      None
    @return     None
    '''
    def __init__(self):
        pass
    

    '''
    @brief      Estimate velocity and pose from IMU and DVL ROS messages
    @param      imuMsg      IMU ROS message passed from LocalizationNode
    @param      dvlMsg      DVL ROS message passed from LocalizationNode
    @param      dt          Time interval used for integration
    @return     velocity    Current velocity vector estimate
    @return     pose        Current pose vector estimate
    @return     R_qw        3x3 NumPy array of world to Qubo frame rotation matrix
    '''
    def estimateState(self, imuMsg, dvlMsg, dt):
        # Extract IMU orientation quaternion
        qx = imuMsg.orientation.x
        qy = imuMsg.orientation.y
        qz = imuMsg.orientation.z
        qw = imuMsg.orientation.w

        if (self.firstPass == True):
            '''
            # Get rotation matrix from world frame to simulation frame if this is the first time the IMU is read
            self.R_sw = quaternion_matrix([qx, qy, qz, qw])[0:3, 0:3]
            self.Rq_sw = [qx, qy, qz, qw]
            '''

            # Set initial IMU accel readings as gravity offset vector
            self.g_w[0][0] = imuMsg.linear_acceleration.x 
            self.g_w[1][0] = imuMsg.linear_acceleration.y
            self.g_w[2][0] = imuMsg.linear_acceleration.z

            self.firstPass = False
        
        '''
        # Get rotation matrix from Qubo frame to simulation frame
        R_sq = quaternion_matrix([qx, qy, qz, qw])[0:3, 0:3]

        # Calculate rotation matrix from world frame to Qubo frame
        R_qs = R_sq.T
        R_qw = np.matmul(R_qs, self.R_sw)
        R_wq = R_qw.T
        '''

        # Get rotation matrices between Qubo and world frames
        R_wq = quaternion_matrix([qx, qy, qz, qw])[0:3, 0:3]
        R_qw = R_wq.T

        # Transform gravity offset column vector from world to Qubo frame
        g_q = np.matmul(R_qw, self.g_w)
        
        # Extract IMU linear acceleration values and offset by g_q components
        xAccel = imuMsg.linear_acceleration.x - g_q[0][0]
        yAccel = imuMsg.linear_acceleration.y - g_q[1][0]
        zAccel = imuMsg.linear_acceleration.z - g_q[2][0]

        '''
        # Extract DVL linear velocity values while tranforming from DVL to Qubo frame
        xRate = dvlMsg.twist.twist.linear.z 
        yRate = dvlMsg.twist.twist.linear.y
        zRate = -dvlMsg.twist.twist.linear.x
        '''

        # Extract DVL linear velocity values
        xRate = dvlMsg.twist.twist.linear.x
        yRate = dvlMsg.twist.twist.linear.y
        zRate = dvlMsg.twist.twist.linear.z

        # Construct derived matrices for Kalman Filter equations
        A = np.array([
            [1, 0, 0, dt, 0, 0],
            [0, 1, 0, 0, dt, 0],
            [0, 0, 1, 0, 0, dt],
            [0, 0, 0, 1, 0, 0],
            [0, 0, 0, 0, 1, 0],
            [0, 0, 0, 0, 0, 1]
        ], dtype=float)

        B = np.concatenate((0.5*(dt**2)*R_wq, dt*R_wq), axis=0)

        C = np.concatenate((np.zeros((3,3)), R_qw), axis=1)

        Q = np.matmul(B, np.matmul(self.Q_a, B.T))

        ### Prediction Step
        u = np.array([[xAccel], [yAccel], [zAccel]])
        self.x = np.matmul(A, self.x) + np.matmul(B, u)
        self.P = np.matmul(A, np.matmul(self.P, A.T)) + Q

        ### Update Step
        y = np.array([[xRate], [yRate], [zRate]])
        K = np.matmul(self.P, np.matmul(C.T, np.linalg.inv(np.matmul(C, np.matmul(self.P, C.T)) + self.R)))
        self.x = self.x + np.matmul(K, y - np.matmul(C, self.x))
        self.P = np.matmul(np.eye(6) - np.matmul(K, C), self.P)

        # Transform state vector velocity component from world frame to Qubo frame
        velocity_w = self.x[3:6][:]
        velocity_q = np.matmul(R_qw, velocity_w)

        # Transfer over linear velocity and pose results from KF calculation
        self.pose[0:3] = self.x[0:3].flatten()
        self.velocity[0:3] = velocity_q[0:3].flatten()

        # Extract angular velocity directly from IMU message
        self.velocity[3] = imuMsg.angular_velocity.y
        self.velocity[4] = imuMsg.angular_velocity.x
        self.velocity[5] = imuMsg.angular_velocity.z

        '''
        # Convert IMU quaternion to represent rotation from Qubo frame to  world frame
        Rq_sq = [qx, qy, qz, qw]
        Rq_qs = quaternion_inverse(Rq_sq)
        Rq_qw = quaternion_multiply(Rq_qs, self.Rq_sw)
        Rq_wq = quaternion_inverse(Rq_qw)
        '''

        # Convert IMU quaternion to angular pose
        (pitchPose, rollPose, yawPose) = euler_from_quaternion([qx, qy, qz, qw])
        self.pose[3] = rollPose
        self.pose[4] = pitchPose
        self.pose[5] = yawPose

        return self.velocity, self.pose, R_qw, self.P



if __name__ == '__main__':
    pass

