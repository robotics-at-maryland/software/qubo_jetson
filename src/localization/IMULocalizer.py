#!/usr/bin/env python
# Required for ROS to execute file as a Python script

'''
@file       IMULocalizer.py
@date       2021/02/27
@brief      State estimator that uses double integration with IMU measurements
'''

# Avoid generating .pyc files
import sys
sys.dont_write_bytecode = True

from collections import deque

from tf.transformations import quaternion_matrix
from tf.transformations import euler_from_quaternion
from tf.transformations import quaternion_inverse
from tf.transformations import quaternion_multiply

import numpy as np


'''
@brief      Qubo state estimator
'''
class IMULocalizer:
    # QUBO FRAME: X=RIGHT, Y=FORWARD, Z=UP
    # Pose is expressed in world frame
    # Velocity is expressed in Qubo frame

    # Rotation matrix from world frame to simulation frame
    # Simulation Frame: Hardcoded frame at the center of the Gazebo pool
    # World Frame: Qubo's frame taken at the beginning of a run
    # NOTE: This matrix should not be needed. Consider changing the IMU output topic.
    R_sw = None

    # R_sw in quaternion form as a list (x,y,z,w)
    Rq_sw = None

    # Gravity offset vector in world frame
    g_w = np.array([[0.0], [0.0], [0.0]])

    # Flag that indicates if R_sw is already initialized
    #firstPass = True
    g_wSampleNumber = 100
    g_wSampleSum = np.zeros((3,1))
    counter = 0
    filterSize = 30
    xAccelQ = deque([0] * filterSize)
    yAccelQ = deque([0] * filterSize)
    zAccelQ = deque([0] * filterSize)
    
    # 6 element current velocity vector (Qubo frame)
    # Order: x,y,z,roll,pitch,yaw
    velocity = \
        np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

    # 6 element current pose vector (world frame)
    # Order: x,y,z,roll,pitch,yaw
    pose = \
        np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])


    '''
    @brief      Class constructor
    @param      None
    @return     None
    '''
    def __init__(self):
        pass
    

    '''
    @brief      Estimate velocity and pose from ROS Imu message
    @param      imuMsge     ROS Imu message passed from LocalizationNode
    @param      dt          Time interval used for integration
    @return     velocity    Current velocity vector estimate
    @return     pose        Current pose vector estimate
    @return     R_qw        3x3 NumPy array of world to Qubo frame rotation matrix
    '''
    def estimateState(self, imuMsg, dt):
        # Extract IMU orientation quaternion
        qx = imuMsg.orientation.x
        qy = imuMsg.orientation.y
        qz = imuMsg.orientation.z
        qw = imuMsg.orientation.w

        '''
        if (self.firstPass == True):
            # Set initial IMU accel readings as gravity offset vector
            self.g_w[0][0] = imuMsg.linear_acceleration.x 
            self.g_w[1][0] = imuMsg.linear_acceleration.y
            self.g_w[2][0] = imuMsg.linear_acceleration.z

            self.firstPass = False
        '''

        print(self.g_w)

        if (self.counter < self.g_wSampleNumber):
            self.g_wSampleSum[0][0] += imuMsg.linear_acceleration.x 
            self.g_wSampleSum[1][0] += imuMsg.linear_acceleration.y
            self.g_wSampleSum[2][0] += imuMsg.linear_acceleration.z

            self.g_w = self.g_wSampleSum / (self.counter + 1)

            self.counter += 1

            return self.velocity, self.pose, np.zeros((3,3))

        else:
            # Get rotation matrices between Qubo and world frames
            R_wq = quaternion_matrix([qx, qy, qz, qw])[0:3, 0:3]
            R_qw = R_wq.T

            # Transform gravitational accel column vector from world to Qubo frame
            # NOTE: The z offset is positive for some reason; need to investigate in the future
            g_q = np.matmul(R_qw, self.g_w)
            
            # Extract IMU linear acceleration values and offset by g_q components
            xAccel = imuMsg.linear_acceleration.x - g_q[0][0]
            yAccel = imuMsg.linear_acceleration.y - g_q[1][0]
            zAccel = imuMsg.linear_acceleration.z - g_q[2][0]

            self.xAccelQ.append(xAccel)
            self.xAccelQ.popleft()
            xAccel = sum(self.xAccelQ) / self.filterSize
            self.yAccelQ.append(xAccel)
            self.yAccelQ.popleft()
            yAccel = sum(self.yAccelQ) / self.filterSize
            self.zAccelQ.append(xAccel)
            self.zAccelQ.popleft()
            zAccel = sum(self.zAccelQ) / self.filterSize

            print(xAccel)
            print(yAccel)
            print(zAccel)

            # Integrate linear acceleration to get linear velocity
            self.velocity[0] += xAccel * dt
            self.velocity[1] += yAccel * dt
            self.velocity[2] += zAccel * dt

            # Transform linear velocity column vector from Qubo to world frame
            linearVelocity_q = np.array([self.velocity[0:3]]).T
            linearVelocity_w = np.matmul(R_wq, linearVelocity_q)

            # Integrate linear velocity to get linear pose
            self.pose[0] += linearVelocity_w[0][0] * dt 	
            self.pose[1] += linearVelocity_w[1][0] * dt	
            self.pose[2] += linearVelocity_w[2][0] * dt

            # Extract angular velocity directly from IMU message
            self.velocity[3] = imuMsg.angular_velocity.y
            self.velocity[4] = imuMsg.angular_velocity.x
            self.velocity[5] = imuMsg.angular_velocity.z

            # Convert IMU quaternion to angular pose
            (pitchPose, rollPose, yawPose) = euler_from_quaternion([qx, qy, qz, qw])
            self.pose[3] = rollPose
            self.pose[4] = pitchPose
            self.pose[5] = yawPose

            return self.velocity, self.pose, R_qw



if __name__ == '__main__':
    pass

