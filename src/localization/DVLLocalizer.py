#!/usr/bin/env python
# Required for ROS to execute file as a Python script

'''
@file       DVLLocalizer.py
@date       2021/03/06
@brief      State estimator that uses integration with DVL measurements
'''

# Avoid generating .pyc files
import sys
sys.dont_write_bytecode = True


from geometry_msgs.msg import TwistWithCovarianceStamped
import numpy as np

from tf.transformations import quaternion_matrix
from tf.transformations import euler_from_quaternion
from tf.transformations import quaternion_inverse
from tf.transformations import quaternion_multiply


'''
@brief      Qubo state estimator
'''
class DVLLocalizer:
    # QUBO FRAME: X=RIGHT, Y=FORWARD, Z=UP
    # Pose is expressed in world frame
    # Velocity is expressed in Qubo frame

    # 6 element current velocity vector
    # Order: x,y,z,roll,pitch,yaw
    velocity = \
        np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

    # 6 element current pose vector
    # Order: x,y,z,roll,pitch,yaw
    pose = \
        np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

    # R_sw in quaternion form as a list (x,y,z,w)
    Rq_sw = None

    # Flag that indicates if R_sw is already initialized
    firstPass = True


    '''
    @brief      Class constructor
    @param      None
    @return     None
    '''
    def __init__(self):
        pass
    

    '''
    @brief      Estimate velocity and pose from ROS DVL message
    @param      imuMsge     ROS Imu message passed from LocalizationNode
    @param      dt          Time interval used for integration
    @return     velocity    Current velocity vector estimate
    @return     pose        Current pose vector estimate
    '''
    def estimateState(self, DVLMsg, imuMsg, dt):



        # Extract IMU orientation quaternion
        qx = imuMsg.orientation.x
        qy = imuMsg.orientation.y
        qz = imuMsg.orientation.z
        qw = imuMsg.orientation.w

        if (self.firstPass == True):
            # Get rotation matrix from world frame to simulation frame if this is the first time the IMU is read
            self.R_sw = quaternion_matrix([qx, qy, qz, qw])[0:3, 0:3]
            self.Rq_sw = [qx, qy, qz, qw]
            self.firstPass = False

        # Get rotation matrix from Qubo frame to simulation frame
        R_sq = quaternion_matrix([qx, qy, qz, qw])[0:3, 0:3]

        # Calculate rotation matrix from world frame to Qubo frame
        R_qs = R_sq.T
        R_qw = np.matmul(R_qs, self.R_sw)
        R_wq = R_qw.T

        # Transform gravitational accel column vector from world to Qubo frame
        # NOTE: The z offset is positive for some reason; need to investigate in the future
        # NOTE: The gravitational vector here is expressed in the ideal simulation frame
        # Need to figure out in the future how to express it in the world frame
        g_s = np.array([[0], [0], [9.8]])
        g_q = np.matmul(R_qs, g_s)




        # Extract linear velocities from DVLMsg

        xRate = DVLMsg.twist.twist.linear.x
        yRate = DVLMsg.twist.twist.linear.y
        zRate = DVLMsg.twist.twist.linear.z


        # Storing extracted values into self.velocity
        self.velocity[0] = xRate
        self.velocity[1] = yRate
        self.velocity[2] = zRate

        #chage self.velocity to be in world coordinates
        self.velocityW = np.matmul(R_wq, self.velocity[:3])

        # Integrate linear velocity to get linear pose
        self.pose[0:3] += self.velocityW * dt 	
        
        # Since the DVL sensor has no orientation information, we are using the IMU to get this information

        # Extract angular velocity directly from IMU message
        self.velocity[3] = imuMsg.angular_velocity.y
        self.velocity[4] = imuMsg.angular_velocity.x
        self.velocity[5] = imuMsg.angular_velocity.z

        # Convert IMU quaternion to represent rotation from Qubo frame to  world frame
        Rq_wq = [qx, qy, qz, qw]

        # Convert IMU quaternion to angular pose
        (pitchPose, rollPose, yawPose) = euler_from_quaternion(Rq_wq)
        self.pose[3] = rollPose
        self.pose[4] = pitchPose
        self.pose[5] = yawPose


        return self.velocity, self.pose, R_qw



if __name__ == '__main__':
    pass