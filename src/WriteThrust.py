import rospy
from std_msgs.msg import Float32MultiArray, MultiArrayLayout, MultiArrayDimension
import math
import numpy as np

from embedded import qubobus
from embedded.qubobus import uart
from embedded.qubobus import qubomessage
from embedded.qubobus import structs

client = None

def writeThrustersCallback(data):

    header = structs.message.MessageHeader(message_type=structs.message.MessageType.request, message_id=50)
    payload = structs.thrusters.ThrusterSet()
    payload.val_1 = data.data[0]
    payload.val_2 = data.data[1]
    payload.val_3 = data.data[2]
    payload.val_4 = data.data[3]
    payload.val_5 = data.data[4]
    payload.val_6 = data.data[5]
    thruster_set_msg = qubomessage.create_message(header=header, payload=payload)
    client.write_message(thruster_set_msg)



if __name__ == "__main__":
    client = uart.QubobusLink()
    client.init_connect('/dev/ttyUSB0')

    rospy.init_node('ThrusterWriter', anonymous=True)
 
    rospy.Subscriber("teleopThrust", Float32MultiArray, writeThrustersCallback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


