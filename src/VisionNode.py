#!/usr/bin/env python
import rospy
import cv2
from sensor_msgs.msg import Image
from std_msgs.msg import Float64MultiArray, MultiArrayLayout, MultiArrayDimension, Float64
from cv_bridge import CvBridge
from vision.gate_detector_v1 import find_gate_old, get_relative_pose, color_stretching
from vision.pipe_detector import find_pipe

class VisionNode:
     
    def pubGateMessage(self, t_vec, r_vec):
        gatePoseMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(t_vec[0], t_vec[2], -t_vec[1], 
                -r_vec[0], -r_vec[2], r_vec[1])
        )#opencv returns swapped y/z compared to qubo frame (documented in localization node)
        self.gatePub.publish(gatePoseMsg)

    def cam_callback(self, data):
        self.imageRaw = self.bridge.imgmsg_to_cv2(data, desired_encoding='passthrough')
        self.image = color_stretching(self.imageRaw)
        gate_found, gates = find_gate_old(self.image, True)
        pipe_found, pipe = find_pipe(self.image, True)
        if gate_found:
            ret, r_vec, t_vec = get_relative_pose(gates)
            self.pubGateMessage(t_vec, r_vec)
        if pipe_found:
            width = 320
            pipeNorm = (pipe-width/2.0)/(width/2.0)
            self.pipePub.publish(pipeNorm)


    def __init__(self, cam_topic="/qubo_gazebo/qubo_gazebo/cameraleft/camera_image", gate_topic="qubo_jetson/gatePose"):
        rospy.init_node('VisionNode', anonymous=False)
        self.bridge = CvBridge()
        self.camSub = rospy.Subscriber(cam_topic, Image, self.cam_callback)
        self.gatePub = rospy.Publisher(gate_topic, Float64MultiArray, queue_size=10)
        self.pipePub = rospy.Publisher("qubo_jetson/pipeXpose", Float64, queue_size=10)


if __name__ == "__main__":
    visionNode = VisionNode()
    rospy.spin()
