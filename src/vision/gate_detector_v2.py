import cv2
import random
import rospy
from sensor_msgs.msg import Image
from nav_msgs.msg import Odometry
from cv_bridge import CvBridge
import numpy as np
import argparse
import math


def norm_image(img):
    imgMax = np.max(img)
    imgMin = np.min(img)
    img -= imgMin
    img = img * (255.0 / (imgMax - imgMin))
    return img.astype(np.uint8)

def getRectContours(contours, areaThresh = 100):
    boxes = []
    for i in range(len(contours[0])):
        rect = cv2.minAreaRect(contours[0][i])
        bbox = np.array(cv2.cv.BoxPoints(rect))
        minX = np.min(bbox[:,0])
        maxX = np.max(bbox[:,0])
        minY = np.min(bbox[:,1])
        maxY = np.max(bbox[:,1])

        area = (maxX - minX) * (maxY - minY)

        if area > areaThresh:
            boxes.append(bbox)

    boxes = np.int32(boxes)
    return boxes

def getDist(x1, y1, x2, y2):
    return math.sqrt((y2-y1)**2 + (x2-x1)**2)

def get_gates_from_boxes(rightBoxes, vertBoxes, distThresh = 20):
    gateList = []
    for i in range(len(rightBoxes)):
        right1MinY = np.min(rightBoxes[i,:,1]) 
        right1MaxY = np.max(rightBoxes[i,:,1]) 
        right1AvgX = np.mean(rightBoxes[i,:,0]) 
        for j in np.arange(i+1, len(rightBoxes)):
            right2MinY = np.min(rightBoxes[j,:,1]) 
            right2MaxY = np.max(rightBoxes[j,:,1]) 
            right2AvgX = np.mean(rightBoxes[j,:,0]) 
            for k in range(len(vertBoxes)):
                vertMinX = np.min(vertBoxes[k,:,0]) 
                vertMaxX = np.max(vertBoxes[k,:,0]) 
                vertMeanY = np.mean(vertBoxes[k,:,1]) 

                comp1XVal = vertMaxX
                comp2XVal = vertMinX
                max1YVal = right2MaxY
                max2YVal = right1MaxY
                if right1AvgX < right2AvgX:
                    comp1XVal = vertMinX 
                    comp2XVal = vertMaxX 
                    max1YVal = right1MaxY
                    max2YVal = right2MaxY

                if (getDist(right1AvgX, right1MinY, comp1XVal, vertMeanY) < distThresh and 
                getDist(right2AvgX, right2MinY, comp2XVal, vertMeanY) < distThresh) :
                    #we have a valid gate
                    gateVals =  [[vertMinX, max1YVal], [vertMinX, vertMeanY], [vertMaxX, vertMeanY], [vertMaxX, max2YVal]]
                    gateList.append(gateVals)

    return np.array(gateList)

def random_color():
    rgbl=[255,0,0]
    random.shuffle(rgbl)
    return tuple(rgbl)

def get_big_gate(gates):
    maxXDist = -1
    maxGate = None
    for gate in gates:
        [[vertMinX, max1YVal], [vertMinX, vertMeanY], [vertMaxX, vertMeanY], [vertMaxX, max2YVal]] = gate
        minX = min(vertMinX, vertMaxX)
        maxX = max(vertMinX, vertMaxX)
        xDist = maxX - minX
        if xDist > maxXDist:
            maxGate = gate
            maxXDist = xDist
    if maxGate is None:
        return np.array([])
    else:
        return np.array([gate])


def find_gate(img, show, filter_thresh = 10):
    blur_img = np.float32(cv2.GaussianBlur(img, (15, 15), cv2.BORDER_DEFAULT))
    orangeImg = (127 - np.abs(127.5 - blur_img[:,:,1])) + blur_img[:,:,2] - blur_img[:,:,0]
    vertKernel = np.array([[1], [0], [-1]]).astype(np.float32)
    rightKernel = np.array([[-1, 0, 1]]).astype(np.float32)

    vertGrad = cv2.filter2D(orangeImg, -1, vertKernel)
    rightGrad = cv2.filter2D(orangeImg, -1, rightKernel)

    vertBin = np.where(vertGrad > filter_thresh, 255, 0).astype(np.uint8)
    rightBin = np.where(rightGrad > filter_thresh, 255, 0).astype(np.uint8)


    edgedRight = cv2.Canny(rightBin, 30, 200)
    edgedVert = cv2.Canny(vertBin, 30, 200)

    rightContours = cv2.findContours(edgedRight, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    vertContours = cv2.findContours(edgedVert, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    rightBoxes = getRectContours(rightContours)
    vertBoxes = getRectContours(vertContours)

    #print(rightBoxes)
    print(img.shape)
    gates = get_gates_from_boxes(rightBoxes, vertBoxes)
    gates = get_big_gate(gates) #only get biggest one, pipe was causing issues
    if show:
        cv2.imshow("Orange Image", cv2.resize(norm_image(orangeImg), (640, 360)))
        cv2.imshow("Vert Grad", cv2.resize(norm_image(vertGrad), (640, 360)))
        cv2.imshow("Vert Bin", cv2.resize(np.uint8(vertBin * 1), (640, 360)))
        cv2.imshow("Right Grad", cv2.resize(norm_image(rightGrad), (640, 360)))
        cv2.imshow("Right Bin", cv2.resize(np.uint8(rightBin * 1), (640, 360)))
        #cv2.drawContours(img, rightContours[1], -1, (0, 255, 0), 3)
        #cv2.drawContours(img, vertContours[1], -1, (0, 0, 255), 3)
        #cv2.drawContours(img, rightBoxes, -1, (0, 255, 0), 3)
        #cv2.drawContours(img, vertBoxes, -1, (0, 0, 255), 3)
        random.seed(10)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        for gate in gates.astype(np.int64):
            cv2.polylines(img, [gate], False, (0, 255, 0), thickness=3)
    return gates, img

class ImgSub:
    def left_callback(self, data):                                              
        self.left_image = self.bridge.imgmsg_to_cv2(data, desired_encoding='passthrough')
        self.avail = True

    def get_next_image(self):
        while not self.avail:
            pass

        self.avail = False
        return self.left_image
        
    def __init__(self, left_topic):
        self.bridge = CvBridge()
        rospy.init_node('GateListener', anonymous = True)
        self.left_sub = rospy.Subscriber(left_topic, Image, self.left_callback)
        self.left_image = None
        self.avail = False

def get_relative_pose(gate):
    mtx = np.array([[407.0646129842357, 0.0, 384.5], [0.0, 407.0646129842357, 246.5], [0.0, 0.0, 1.0]])
    dist = np.array([0.0, 0.0, 0.0, 0.0, 0.0])
    objp = np.array([[-1.05, -.75, 0], [-1.05, .8, 0], [1.05, .8, 0], [1.05, -.75, 0]])
    ret,rvecs, tvecs = cv2.solvePnP(objp, gate, mtx, dist)
    return ret, rvecs, tvecs

def PoseGTCallback(data):
    pose = data.pose.pose.position
    gt_pose = np.array([pose.x,pose.y, pose.z])

if __name__ == "__main__":
    ap=argparse.ArgumentParser()
    ap.add_argument("-v", "--video", required=False, help="path to input video")
    args = vars(ap.parse_args())
    cap = None
    cameraSub = None
    if args["video"]:
        cap = cv2.VideoCapture(args["video"])
    else:
        cameraSub = ImgSub("/qubo_gazebo/qubo_gazebo/cameraleft/camera_image")
        poseGTSub = rospy.Subscriber("/qubo_gazebo/pose_gt", Odometry, PoseGTCallback)
                
    frame_counter = 0
    while True:
        frame_counter += 1 #If the last frame is reached, reset the capture and the frame_counter
        if args["video"]:
            if frame_counter == cap.get(cv2.CAP_PROP_FRAME_COUNT):
                frame_counter = 0
                cap.set(cv2.CAP_PROP_POS_FRAMES, 0)
                ret, frame = cap.read()
        else:
            frame = cameraSub.get_next_image()
        
        gates, img = find_gate(frame, True) #currently this only works for 1 gate and will find the first valid gate
        print(gates)
        if len(gates) > 0:
            ret, r_vec, t_vec_cam = get_relative_pose(gates[0])
            #r_vec = r_vec[[0,2,1]]#opencv has y vertical, we have y forward
            #t_vec = t_vec[[0,2,1]]
            #r_vec[[0,1]] *= -1
            #t_vec[2] *= -1
            np_rodrigues = np.asarray(r_vec[:,:],np.float64)
            rot_matrix = cv2.Rodrigues(np_rodrigues)[0]
            t_vec = -np.matrix(rot_matrix).T * np.matrix(t_vec_cam)

            print ("R {}".format(r_vec))
            print ("T {}".format(t_vec))
            print ("T Old{}".format(t_vec_cam))

            print("")
            print(t_vec[0, 0])
            cv2.putText(img, "Rel. Pose X: {:.3f} m, Y: {:.3f}m, Z: {:.3f}m".format(t_vec[0, 0], t_vec[1, 0], t_vec[2, 0]), (30, 30), cv2.FONT_HERSHEY_SIMPLEX, .7, (0, 0, 255), 1, cv2.CV_AA)

            cv2.putText(img, "Rel. Rot. X: {:.3f} rad, Y: {:.3f}rad, Z: {:.3f}rad".format(float(r_vec[0, 0]), float(r_vec[1, 0]), float(r_vec[2, 0])), (30, 60), cv2.FONT_HERSHEY_SIMPLEX, .7, (0, 0, 255), 1, cv2.CV_AA)
        cv2.imshow("contours", cv2.resize(img, (640, 360)))
		
        if cv2.waitKey(1) == ord('q'):
            break;
		
    cap.release()
    cv2.destroyAllWindows()
