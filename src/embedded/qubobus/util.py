def print_hex(b):
    print(" ".join("{:02x}".format(x) for x in bytearray(b)))
