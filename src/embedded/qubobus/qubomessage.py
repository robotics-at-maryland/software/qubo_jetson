import ctypes
import sys

# Local imports
from structs import message
import util

class Message(object):
    header = None
    payload = None
    footer = None

    # This class initializer only puts the data into a convenient object with
    # useful methods. It should not be used directly when sending messages, as
    # there are some things that should be automatically calculated when
    # creating and sending a message.
    def __init__(self, header, payload=None, footer=None):
        assert isinstance(header, message.MessageHeader)
        # assert isinstance(payload, )

        self.header = header
        self.payload = payload
        self.footer = footer

    # Generate footer with checksum of header and payload
    def generate_footer(self):
        self.footer = message.MessageFooter(checksum=self.crc16())

    # @return computed checksum of message header and payload
    def crc16(self):
        checksum = 0
        msg = bytearray(self.header)
        if self.payload is not None:
            msg += bytearray(self.payload)
        for b in msg:
            checksum += b
        return checksum

    # Return the message as bytes ready to be sent over the wire.
    def serialize(self):
        # print("BEGIN serializing message")
        ret = None
        ret = bytearray(self.header)
        if self.payload is not None:
            ret += bytearray(self.payload)
        ret += bytearray(self.footer)
        # print("header.num_bytes: %d" % self.header.num_bytes)
        # print("header.message_type: %d" % self.header.message_type)
        # print("header.message_id: %d" % self.header.message_id)
        # print("header.sequence_number: %d" % self.header.sequence_number)
        # print("END serializing message")
        sys.stdout.write("serialize: ")
        util.print_hex(ret)
        sys.stdout.flush()
        return ret

class Announce(Message):
    def __init__(self):
        payload = None
        header = message.MessageHeader(
            message_type=message.MessageType.announce.value,
            message_id=0,
            )
        super(Announce, self).__init__(header, payload)

# create_message() is for use when sending messages.
def create_message(header, payload=None):
    assert isinstance(header, message.MessageHeader)

    msg = Message(header=header, payload=payload)

    msg.header.num_bytes = ctypes.sizeof(message.MessageHeader)
    # Payload will be None for announce messages, which just have a header and
    # footer.
    if payload is not None:
        msg.payload = payload
        msg.header.num_bytes += ctypes.sizeof(msg.payload)
    msg.header.num_bytes += ctypes.sizeof(message.MessageFooter)
    print("header bytes: %d" % msg.header.num_bytes)

    # Footer does not exist yet. It will be generated when sending the message,
    # as there is some more data that needs to be added that is not accessibly
    # here, like the sequence number.

    return msg

def create_announce():
    header = message.MessageHeader(
        message_type=message.MessageType.announce.value,
        message_id=0,
        )

    return create_message(header=header, payload=None)
