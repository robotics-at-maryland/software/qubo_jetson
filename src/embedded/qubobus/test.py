import uart
import qubomessage
import structs

port = '/dev/ttyUSB0'

l = uart.QubobusLink()
l.init_connect(port)

# wait for user to indicate to continue
# raw_input("Press enter to continue...")


import time
import random

while True:
    s = time.time()
    time.sleep(0.005)
    header = structs.message.MessageHeader(message_type=structs.message.MessageType.request, message_id=50)
    payload = structs.thrusters.ThrusterSet()
    payload.val_1 = random.random() * 10
    payload.val_2 = random.random() * 10
    payload.val_3 = random.random() * 10
    payload.val_4 = random.random() * 10
    payload.val_5 = random.random() * 10
    payload.val_6 = random.random() * 10
    thruster_set_msg = qubomessage.create_message(header=header, payload=payload)
    l.write_message(thruster_set_msg)
    print(time.time() - s)
