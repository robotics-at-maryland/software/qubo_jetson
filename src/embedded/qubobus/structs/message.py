from enum import IntEnum, unique
from ctypes import *

@unique
class MessageType(IntEnum):
    null = 0
    announce = 1
    protocol = 2
    keep_alive = 3
    request = 4
    response = 5
    error = 6

    max = 7 # Invalid message ID, bookkeeping for the maximum of message types.

class MessageHeader(Structure):
    _fields_ = [("num_bytes", c_uint16), ("message_type", c_uint8), ("message_id", c_uint8), ("sequence_number", c_uint16)]

class MessageFooter(Structure):
    _fields_ = [("checksum", c_uint16)]

class ProtocolInfo(Structure):
    _fields_ = [("version", c_uint16)]
