from ctypes import *

class ThrusterSet(Structure):
    _fields_ = ("val_1", c_float), ("val_2", c_float), ("val_3", c_float), ("val_4", c_float), ("val_5", c_float), ("val_6", c_float), ("type", c_int)
