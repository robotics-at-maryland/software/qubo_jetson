from ctypes import *

RAW_IO_FUNCTION = CFUNCTYPE(c_ssize_t, c_void_p, c_void_p, c_size_t)

class IOState(Structure):
    _fields_ = ("io_host", c_void_p), ("read_raw", RAW_IO_FUNCTION), ("write_raw", RAW_IO_FUNCTION), ("local_sequence_number", c_uint16), ("remote_sequence_number", c_uint16)

class DepthStatus(Structure):
    _fields_ = ("depth_m", c_float), ("warning_level", c_uint8)
