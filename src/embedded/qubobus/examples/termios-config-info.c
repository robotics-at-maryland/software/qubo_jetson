/*
 * File: `termios-config-info.c`
 * Authors: Jeffrey Fisher <qubo@jgfisher.fastmail.com>
 *
 * This program prints some values from `termios.h` in hexadecimal in order to
 * help visualize how the UART code configures the terminal settings.
 *
 * This program is an example. It is purely for illustrative and educational
 * purposes, it has no bearing on the correctness of the code that runs on
 * Qubo.
 */

#include <stdio.h>
#include <termios.h>

int
main(void)
{
	/*
	 * Print each value in hexadecimal, left-padded with zeroes up to their
	 * size. `sizeof` returns a number of bytes, one byte is represented by
	 * two hexadecimal digits.
	 */
	printf("CSTOPB:   x%0*x\n", (int)(2 * sizeof(CSTOPB)), CSTOPB);
	printf("PARENB:   x%0*x\n", (int)(2 * sizeof(PARENB)), PARENB);
	printf("CS8:      x%0*x\n", (int)(2 * sizeof(CS8)), CS8);
	printf("CSIZE:    x%0*x\n", (int)(2 * sizeof(CSIZE)), CSIZE);

	return 0;
}
