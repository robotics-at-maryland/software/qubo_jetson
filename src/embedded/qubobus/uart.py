# Standard library
import sys
import ctypes
from ctypes import CFUNCTYPE, c_ssize_t, c_void_p, c_size_t, POINTER

# Third party libraries
import serial

# Local imports
import structs
import qubomessage
import util

# ssize_t foo(void*, void*, size_t)
RAW_IO_FUNCTION = CFUNCTYPE(c_ssize_t, c_void_p, c_void_p, c_size_t)

class QubobusLink:
    # State
    uart = None # pySerial Serial object / connection

    # Protocol State
    sequence_number = None
    remote_sequence_number = None

    # Constants
    QUBOBUS_PROTOCOL_VERSION = 3
    BAUDRATE = 115200
    PRIORITY = 80 # Not quite sure what this does
    MAX_MESSAGE_SIZE = 128 # TODO: Double check
    MAX_PAYLOAD_SIZE = 64 # TODO: Double check
    BUFFER_SIZE = (MAX_MESSAGE_SIZE + MAX_PAYLOAD_SIZE)
    # READ_TIMEOUT = 1 # unit: seconds
    # WRITE_TIMEOUT = 1 # unit: seconds
    READ_TIMEOUT = None # unit: seconds
    WRITE_TIMEOUT = None # unit: seconds

    def __init__(self):
        assert sys.byteorder == 'little'

        self.sequence_number = 40

    '''
    @brief Open UART connection
    @param port UART serial device file path
    '''
    def init_connect(self, port):
        # We also use the following settings, but pySerial seems to work fine
        # without specifying them:
        # - 8 data bits
        # - 1 stop bit
        # - no parity bits
        self.uart = serial.Serial(port=port, baudrate=self.BAUDRATE,
                timeout=self.READ_TIMEOUT, write_timeout=self.WRITE_TIMEOUT, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE)

        # TODO define create_message function that is easier to use, computers
        # checksum for footer automatically. Maybe have separate functions for
        # creating each type of message, e.g. a thruster packet. Could be
        # subclasses of Message and their __init__() could handle everything.
        # our_announce = structs.Message(header=structs.message.MessageHeader(
        #             message_type=structs.message.MessageType.protocol,
        #             message_id=SOME_MESSAGE_ID,
        #             sequence_number=sequence_number),
        #         payload=None,
        #         footer=structs.message.MessageFooter(checksum=10),
        #         )
        our_announce = qubomessage.create_announce()

        # Send announce message.
        self.write_message(our_announce)
        print("Wrote Announce message")

        # Read their announce message.
        print("Begin reading Tiva announce message")
        their_announce = self.read_announce()
        print("Finished reading Tiva announce message")

        # Save their sequence number.
        remote_sequence_number = their_announce.header.sequence_number

        #
        # NEGOTIATE PROTOCOL
        #

        # Send protocol message
        self.send_protocol_info()

        # Read their protocol message.
        their_proto = self.read_message()

        # TODO: Add additional checks that C init_connect() does, like ensure
        # protocol version is the same for us and our conversation partner.

    def send_protocol_info(self):
        protocol_info = structs.message.ProtocolInfo(version=self.QUBOBUS_PROTOCOL_VERSION)
        header = structs.message.MessageHeader(message_type=structs.message.MessageType.protocol, message_id=0)
        msg = qubomessage.create_message(header=header, payload=protocol_info)

        self.write_message(msg)

    # Write a message over UART
    def write_message(self, msg):
        # Increment `sequence_number`, set sequence number in message header.
        self.sequence_number += 1
        msg.header.sequence_number = self.sequence_number
        msg.generate_footer()

        # TODO handle timeouts. Need to coordinate with Tiva side so that they
        # work properly together.
        print("BEGIN writing message")
        ser = msg.serialize()
        # print(repr(ser))
        self.uart.write(ser);
        print("END writing message")

    def read_announce(self):
        # Read message header
        header = structs.message.MessageHeader()
        raw = self.uart.read(ctypes.sizeof(header))
        print("raw header len: %d" % len(raw))
        # util.print_hex(bytearray(raw))
        ctypes.memmove(ctypes.byref(header), raw,
                ctypes.sizeof(header))

        # There is no payload

        # Read message footer
        footer = structs.message.MessageFooter()
        raw = self.uart.read(ctypes.sizeof(footer))
        print("raw footer len: %d" % len(raw))
        ctypes.memmove(ctypes.byref(footer), raw, ctypes.sizeof(footer))

        msg = qubomessage.Message(header=header, footer=footer)

        return msg

    # Read a message from UART
    def read_message(self):
        # TODO Serial.read_until() may be very useful, effectively acting like
        # Qubobus' safe_io(), but we don't have to write it. We will have to
        # decide what we want in terms of timeouts and error handling.

        # Read message header, check size of the message we are receiving.
        # TODO Somehow cast or load the bytes into the ctypes struct object
        header = structs.message.MessageHeader()
        raw = self.uart.read(ctypes.sizeof(header))
        ctypes.memmove(ctypes.byref(header), raw,
                ctypes.sizeof(header))

        # Read message payload.
        payload_size = (
            header.num_bytes
            - ctypes.sizeof(structs.message.MessageHeader)
            - ctypes.sizeof(structs.message.MessageFooter)
            )
        # TODO handle timeouts. Need to coordinate with Tiva side so that they
        # work properly together.
        print("read_message payload size: %d" % payload_size)
        payload = self.uart.read(payload_size)
        print("read_message read %d bytes" % len(payload))

        # Read message footer
        footer = structs.message.MessageFooter()
        raw = self.uart.read(ctypes.sizeof(footer))
        ctypes.memmove(ctypes.byref(footer), raw, ctypes.sizeof(footer))

        # TODO: Add error handling, check crc16 in footer
        msg = qubomessage.Message(header=header, payload=payload, footer=footer)

        return msg

    # BEGIN Raw IO functions
    # END Raw IO functions
