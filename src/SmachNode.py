#!/usr/bin/env python
# Required for ROS to execute file as a Python script

from smach import autonomyTasks
import rospy

from std_msgs.msg import String
from nav_msgs.msg import Odometry
from std_msgs.msg import Float64MultiArray, MultiArrayDimension, MultiArrayLayout


from tf.transformations import euler_from_quaternion

import numpy as np


class SMACHNode:
    statePub = None
    desiredPosePub = None
    rate = None

    state = 'Initialization'
    gatePassFlag = False

    gateState = "toClose" #toClose -> toTarget -> toFar

    currentPose = None


    def __init__(self):
        rospy.init_node('smachNode', anonymous=False)
        self.autoTasks = autonomyTasks.autonomyTasks()
        self.statePub = rospy.Publisher('state', String, queue_size=10)
        self.desiredPosePub = rospy.Publisher('qubo_jetson/desiredPose', Float64MultiArray, queue_size=1)
        rospy.Subscriber("qubo_jetson/currentPose", Float64MultiArray, self.poseCB)
        rospy.Subscriber("qubo_jetson/currentVelocity", Float64MultiArray, self.velCB)

        self.estimatedGateClosePosSub = rospy.Subscriber("qubo_jetson/globalGateClosePos", Float64MultiArray, self.gateClosePos_callback)
        self.estimatedGateTargetPosSub = rospy.Subscriber("qubo_jetson/worldGatePose", Float64MultiArray, self.gateTargetPos_callback)
        self.estimatedGateFarPosSub = rospy.Subscriber("qubo_jetson/globalGateFarPos", Float64MultiArray, self.gateFarPos_callback)


        #rospy.Subscriber("gazebo_qubo/pose_gt", Odometry, self.odomCB, queue_size = 1)
        self.rate = rospy.Rate(10)   # 10hz
        self.init_time = rospy.get_time()
    
    def poseCB(self, data):
        data = data.data
        self.currentPose = np.array([data[0], data[1], data[2], data[3], data[4], data[5]])

    def velCB(self, velMsg):
        self.currentVelocity = velMsg.data

    def odomCB(self, poseMsg):
        x = poseMsg.pose.pose.position.x
        y = poseMsg.pose.pose.position.y
        z = poseMsg.pose.pose.position.z

        qx = poseMsg.pose.pose.orientation.x
        qy = poseMsg.pose.pose.orientation.y
        qz = poseMsg.pose.pose.orientation.z
        qw = poseMsg.pose.pose.orientation.w

        euler = euler_from_quaternion((qx, qy, qz, qw))
        roll = euler[0]
        pitch = euler[1]
        yaw = euler[2]

        self.currentPose = np.array([[x], [y], [z], [roll], [pitch], [yaw]])
        print(self.currentPose)


    def initialization(self):
        self.state = 'Initialization'
        rospy.loginfo(self.state)
        self.statePub.publish(self.state)
        self.rate.sleep()

        quit_init = rospy.get_time()
        if quit_init:
            self.state = 'Gate Search'
            self.startTime = rospy.get_rostime()
    

    def debug(self):
        self.state = 'Debug'
        rospy.loginfo(self.state)
        self.statePub.publish(self.state)
        self.rate.sleep()

        # Dummy condition generator
        condition = raw_input('Debug complete? Enter y or n')

        if (condition == 'y'):
            self.state = 'Initialization'
        else:
            self.state = 'Debug'
    

    def gateSearch(self):
        self.state = 'Gate Search'
        rospy.loginfo(self.state)
        self.statePub.publish(self.state)
        self.rate.sleep()

        
        breakTime = 20

        #15 seconds elapsed
        condition = (rospy.get_rostime() - self.startTime).secs > 4

        if (condition):
            self.state = 'Gate Navigation'
        else:
            self.state = 'Gate Search'
            self.gateState = "toClose"
        self.i =0
    

    def gateNavigation(self):
        self.state = 'Gate Navigation'
        rospy.loginfo(self.state)
        self.statePub.publish(self.state)
        self.i += 1

        #gate
        if(self.gateState == "toClose"):
            if True or (self.autoTasks.checkStatePosition(self.currentPose, self.gateClosePose)):
                self.gateState = "toTarget"
            else:
                #self.gateState = "toClose"
                #starting with only going to middle of gate for simplicity
                self.gateState = "toTarget"

        elif(self.gateState == "toTarget"):
            if (self.autoTasks.checkStatePosition(self.gateTargetPose, self.currentPose)):
                self.gateState = "toFar"
            else:
                self.gateState = "toTarget"

        elif(self.gateState == "toFar"):
            if( True or self.autoTasks.checkStatePosition(self.currentPose, self.gateFarPose)):
                #self.gateState = "toFar"
                #for first iter. just have it go to middle of gate, then skip to pol search
                self.gateState = "Done"
                self.state = "Pole Move"
            else:
                self.state = 'Pole Search'
        desiredPose = self.gateTargetPose
        desiredPoseMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(desiredPose[0], desiredPose[1], desiredPose[2], 
                0, 0, 0)
        )
        self.desiredPosePub.publish(desiredPoseMsg)

    def poleMove(self):
        self.state = 'Pole Move'
        self.gatePipeYDelta = 3.2
        rospy.loginfo(self.state)
        self.statePub.publish(self.state)
        desiredPose = np.array(self.gateTargetPose)
        desiredPose[1] = self.gateTargetPose[1] + self.gatePipeYDelta
        if (self.autoTasks.checkStatePosition(desiredPose, self.currentPose)):
            self.state = "Pole Circle"
        desiredPoseMsg = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(desiredPose[0], desiredPose[1], desiredPose[2], 
                0, 0, 0)
        )
        print("Current Pose then Desired Pose")
        print(self.currentPose)
        print(desiredPose)
        print(self.gateTargetPose)
        self.desiredPosePub.publish(desiredPoseMsg)
        self.rate.sleep()

    def poleCircle(self):
        self.state = 'Pole Circle'
        self.statePub.publish(self.state)
        self.rate.sleep()

    def breachAndShutdown(self):
        self.state = 'Breach and Shutdown'
        rospy.loginfo(self.state)
        self.statePub.publish(self.state)
        self.rate.sleep()


    def run(self):
        self.statePub.publish(self.state)
        while not rospy.is_shutdown():
            if (self.state == 'Initialization'):
                self.initialization()

            elif (self.state == 'Debug'):
                self.debug()

            elif (self.state == 'Gate Search'):
                self.gateSearch()

            elif (self.state == 'Gate Navigation'):
                self.gateNavigation()

            elif (self.state == 'Pole Move'):
                self.poleMove()

            elif (self.state == 'Pole Navigation'):
                self.poleNavigation()

            elif (self.state == 'Breach and Shutdown'):
                self.breachAndShutdown()
                break

            else:
                rospy.loginfo('Invalid State!')
                self.rate.sleep()
                break

    def gateClosePos_callback(self, data):
        self.gateClosePose = np.array([data[0], data[1], data[2], data[3], data[4], data[5]])

    def gateTargetPos_callback(self, data):
        data = data.data
        if self.state == 'Gate Search':
        	self.gateTargetPose = np.array([data[0], data[1], data[2], data[3], data[4], data[5]])
        print(self.gateTargetPose)

    def gateFarPos_callback(self, data):
        self.gateFarPose = np.array([data[0], data[1], data[2], data[3], data[4], data[5]])

        

if __name__ == '__main__':
    smachNode = SMACHNode()

    try:
        smachNode.run()
    except rospy.ROSInterruptException:
        pass
