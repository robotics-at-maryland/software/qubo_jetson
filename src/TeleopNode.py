#!/usr/bin/env python
# Required for ROS to execute file as a Python script

'''
@file       TeleopNode.py
@date       2021/02/27
@brief      Qubo teleop node for velocity and pose commands
'''


import math
import pygame

import rospy
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension
from geometry_msgs.msg import Wrench
from geometry_msgs.msg import Vector3


'''
@brief      Qubo teleoperation node
'''
class TeleopNode:
    # QUBO FRAME: X=RIGHT, Y=FORWARD, Z=UP

    thrusterPub = None          # Thruster commands publisher
    desiredVelocityPub = None   # Desired velocity publisher
    desiredPosePub = None       # Desired pose publisher
    rate = None                 # ROS publishing rate object

    xInput = 0                  # Desired linear x thruster input
    yInput = 0                  # Desired linear y thruster input
    zInput = 0                  # Desired linear z thruster input
    rollInput = 0               # Desired roll thruster input
    pitchInput = 0              # Desired pitch thruster input
    yawInput = 0                # Desired yaw thruster input

    xRate = 0                   # Desired linear x rate
    yRate = 0                   # Desired linear y rate
    zRate = 0                   # Desired linear z rate
    rollRate = 0                # Desired roll rate
    pitchRate = 0               # Desired pitch rate
    yawRate = 0                 # Desired yaw rate

    xPose = 0                   # Desired linear x pose
    yPose = 0                   # Desired linear y pose
    zPose = 0                   # Desired linear z pose
    rollPose = 0                # Desired roll pose
    pitchPose = 0               # Desired pitch pose
    yawPose = 0                 # Desired yaw pose

    prevKeyStates = None        # Variable to keep track of previous key press states

    index = 0                   # Index for cycling through pose sequence


    '''
    @brief      Class constructor
    @param      None
    @return     None
    '''
    def __init__(self):
        # Initialize ROS node
        rospy.init_node('TeleopNode', anonymous=False)

        # Set publishing rate
        self.rate = rospy.Rate(50)

        # Initialize PyGame
        pygame.init()
        pygame.display.set_mode([100, 100])
        pygame.key.set_repeat(10, 10)

        # Get initial key states
        self.prevKeyStates = pygame.key.get_pressed()

        # Get teleop mode from ROS node input
        teleopMode = rospy.get_param('teleopMode')

        # Initialize different publishers and run functions based on the mode
        if (teleopMode == 'manual'):
            self.thrusterPub = rospy.Publisher('qubo_gazebo/thruster_manager/input', Wrench, queue_size=1)
            self.manualTeleop()

        elif (teleopMode == 'velocity'):
            # Force a ControlsNode to use velocityPID in this mode
            rospy.set_param('controlMode', 'velocityPID')
            self.desiredVelocityPub = rospy.Publisher('qubo_jetson/desiredVelocity', Float64MultiArray, queue_size=1)
            self.velocityTeleop()

        elif (teleopMode == 'pose'):
            # Force a ControlsNode to use posePID in this mode
            rospy.set_param('controlMode', 'posePID')
            self.desiredPosePub = rospy.Publisher('qubo_jetson/desiredPose', Float64MultiArray, queue_size=1)
            self.poseTeleop()
        
        elif (teleopMode == 'poseSequence'):
            # Force a ControlsNode to use posePID in this mode
            rospy.set_param('controlMode', 'posePID')
            poseSequence = rospy.get_param('poseSequence')
            self.desiredPosePub = rospy.Publisher('qubo_jetson/desiredPose', Float64MultiArray, queue_size=1)
            self.poseSequenceTeleop(poseSequence)

        else:
            # Shut down node if an invalid control mode is provided
            rospy.signal_shutdown('Invalid localizer mode')


    '''
    @brief      Manual (direct thruster) teleop function
    @param      None
    @return     None
    '''
    def manualTeleop(self):
        # Continue to poll key states while ROS is active
        while (rospy.is_shutdown() == False):
            keyStates = pygame.key.get_pressed()

            # Apply thruster inputs forwards or backwards based on key press
            if (keyStates[pygame.K_w]):
                self.yInput = 0.5
            elif (keyStates[pygame.K_s]):
                self.yInput = -0.5

            elif (keyStates[pygame.K_d]):
                self.xInput = 0.5
            elif (keyStates[pygame.K_a]):
                self.xInput = -0.5

            elif (keyStates[pygame.K_UP]):
                self.zInput = 0.5
            elif (keyStates[pygame.K_DOWN]):
                self.zInput = -0.5

            elif (keyStates[pygame.K_LEFT]):
                self.yawInput = 0.5
            elif (keyStates[pygame.K_RIGHT]):
                self.yawInput = -0.5

            elif (keyStates[pygame.K_l]):
                self.rollInput = 0.5
            elif (keyStates[pygame.K_j]):
                self.rollInput = -0.5

            elif (keyStates[pygame.K_k]):
                self.pitchInput = 0.5
            elif (keyStates[pygame.K_i]):
                self.pitchInput = -0.5
            
            else:
                self.xInput = 0
                self.yInput = 0
                self.zInput = 0
                self.rollInput = 0
                self.pitchInput = 0
                self.yawInput = 0
            
            # Process PyGame event queue
            pygame.event.pump()
	    
            # Construct thruster commands message
            thrusterInputMsg = \
            Wrench(
                force=Vector3(x=self.xInput, y=self.yInput, z=self.zInput), 
		        #force = Vector3(x=xInput, y =0, z=0),
                torque=Vector3(x=self.pitchInput, y=self.rollInput, z=self.yawInput)
            )

            # Publish thruster command message
            #print('Thruster Command Vector: ')
            #print(thrusterInputMsg)
            self.thrusterPub.publish(thrusterInputMsg)
            self.rate.sleep()


    '''
    @brief      Velocity teleop function
    @param      None
    @return     None
    '''
    def velocityTeleop(self):
        # Continue to poll key states while ROS is active
        while (rospy.is_shutdown() == False):
            keyStates = pygame.key.get_pressed()
                    
            # Increment or decrement Qubo velocities based on key press
            # Only change a velocity when a key transitions from not pressed to pressed
            if (keyStates[pygame.K_w] and self.prevKeyStates[pygame.K_w]==False):
                self.yRate += 1
            elif (keyStates[pygame.K_s] and self.prevKeyStates[pygame.K_s]==False):
                self.yRate -= 1

            elif (keyStates[pygame.K_d] and self.prevKeyStates[pygame.K_d]==False):
                self.xRate += 1
            elif (keyStates[pygame.K_a] and self.prevKeyStates[pygame.K_a]==False):
                self.xRate -= 1

            elif (keyStates[pygame.K_UP] and self.prevKeyStates[pygame.K_UP]==False):
                self.zRate += 1
            elif (keyStates[pygame.K_DOWN] and self.prevKeyStates[pygame.K_DOWN]==False):
                self.zRate -= 1

            elif (keyStates[pygame.K_LEFT] and self.prevKeyStates[pygame.K_LEFT]==False):
                self.yawRate += math.pi / 4
            elif (keyStates[pygame.K_RIGHT] and self.prevKeyStates[pygame.K_RIGHT]==False):
                self.yawRate -= math.pi / 4

            elif (keyStates[pygame.K_l] and self.prevKeyStates[pygame.K_l]==False):
                self.rollRate += math.pi / 4
            elif (keyStates[pygame.K_j] and self.prevKeyStates[pygame.K_j]==False):
                self.rollRate -= math.pi / 4

            elif (keyStates[pygame.K_k] and self.prevKeyStates[pygame.K_k]==False):
                self.pitchRate += math.pi / 4
            elif (keyStates[pygame.K_i] and self.prevKeyStates[pygame.K_i]==False):
                self.pitchRate -= math.pi / 4

            self.prevKeyStates = keyStates

            # Process PyGame event queue
            pygame.event.pump()

            # Construct very convoluted 6-element 1D vector ROS message
            desiredVelMsg = Float64MultiArray(
                layout=MultiArrayLayout(
                    dim=(MultiArrayDimension(
                        label='dim1',
                        size=6,
                        stride=6
                    ),),
                    data_offset=0
                ),
                data=(self.xRate, self.yRate, self.zRate, 
                    self.rollRate, self.pitchRate, self.yawRate)
            )

            # Publish desired velocity message
            print('Desired Velocity Vector: ')
            print(desiredVelMsg.data)
            self.desiredVelocityPub.publish(desiredVelMsg)
            self.rate.sleep()
    

    '''
    @brief      Pose teleop function
    @param      None
    @return     None
    '''
    def poseTeleop(self):
        # Continue to poll key states while ROS is active
        while (rospy.is_shutdown() == False):
            keyStates = pygame.key.get_pressed()

            # Increment or decrement Qubo pose values based on key press
            # Only change an value when a key transitions from not pressed to pressed
            if (keyStates[pygame.K_w] and self.prevKeyStates[pygame.K_w]==False):
                self.yPose += 1
            elif (keyStates[pygame.K_s] and self.prevKeyStates[pygame.K_s]==False):
                self.yPose -= 1

            elif (keyStates[pygame.K_d] and self.prevKeyStates[pygame.K_d]==False):
                self.xPose += 1
            elif (keyStates[pygame.K_a] and self.prevKeyStates[pygame.K_a]==False):
                self.xPose -= 1

            elif (keyStates[pygame.K_UP] and self.prevKeyStates[pygame.K_UP]==False):
                self.zPose += 1
            elif (keyStates[pygame.K_DOWN] and self.prevKeyStates[pygame.K_DOWN]==False):
                self.zPose -= 1

            elif (keyStates[pygame.K_LEFT] and self.prevKeyStates[pygame.K_LEFT]==False):
                self.yawPose += math.pi / 4
            elif (keyStates[pygame.K_RIGHT] and self.prevKeyStates[pygame.K_RIGHT]==False):
                self.yawPose -= math.pi / 4

            elif (keyStates[pygame.K_l] and self.prevKeyStates[pygame.K_l]==False):
                self.rollPose += math.pi / 4
            elif (keyStates[pygame.K_j] and self.prevKeyStates[pygame.K_j]==False):
                self.rollPose -= math.pi / 4

            elif (keyStates[pygame.K_k] and self.prevKeyStates[pygame.K_k]==False):
                self.pitchPose += math.pi / 4
            elif (keyStates[pygame.K_i] and self.prevKeyStates[pygame.K_i]==False):
                self.pitchPose -= math.pi / 4

            self.prevKeyStates = keyStates

            # Process PyGame event queue
            pygame.event.pump()

            # Construct very convoluted 6-element 1D vector ROS message
            desiredPoseMsg = Float64MultiArray(
                layout=MultiArrayLayout(
                    dim=(MultiArrayDimension(
                        label='dim1',
                        size=6,
                        stride=6
                    ),),
                    data_offset=0
                ),
                data=(self.xPose, self.yPose, self.zPose, 
                    self.rollPose, self.pitchPose, self.yawPose)
            )

            # Publish desired pose message
            print('Desired Pose Vector: ')
            print(desiredPoseMsg.data)
            self.desiredPosePub.publish(desiredPoseMsg)
            self.rate.sleep()


    '''
    @brief      Teleop using a sequence of predefined poses
    @param      poseSequence    Nx6 nested list of poses to cycle through
    @return     None
    '''
    def poseSequenceTeleop(self, poseSequence):
        # Continue to poll key states while ROS is active
        while (rospy.is_shutdown() == False):
            keyStates = pygame.key.get_pressed()
                    
            # Change to the next desired pose by pressing "W"
            # Only change when a key transitions from not pressed to pressed
            if (keyStates[pygame.K_w] and self.prevKeyStates[pygame.K_w]==False):
                # Extract next desired pose from sequence
                self.xPose = poseSequence[self.index][0]
                self.yPose = poseSequence[self.index][1]
                self.zPose = poseSequence[self.index][2]
                self.rollPose = poseSequence[self.index][3]
                self.pitchPose = poseSequence[self.index][4]
                self.yawPose = poseSequence[self.index][5]

                # Increment index cyclically 
                self.index = (self.index+1) % len(poseSequence)

            self.prevKeyStates = keyStates
            
            # Construct very convoluted 6-element 1D vector ROS message
            desiredPoseMsg = Float64MultiArray(
                layout=MultiArrayLayout(
                    dim=(MultiArrayDimension(
                        label='dim1',
                        size=6,
                        stride=6
                    ),),
                    data_offset=0
                ),
                data=(self.xPose, self.yPose, self.zPose, 
                    self.rollPose, self.pitchPose, self.yawPose)
            )

            # Publish desired pose message
            print('Desired Pose Vector: ')
            print(desiredPoseMsg.data)
            self.desiredPosePub.publish(desiredPoseMsg)
            self.rate.sleep()
            
            # Process PyGame event queue
            pygame.event.pump()



if __name__ == '__main__':
    teleopNode = TeleopNode()
    rospy.spin()

