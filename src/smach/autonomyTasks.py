#A class of various autonomy tasks to be used in the smach node

#!/usr/bin/env python
from numpy.core.numeric import roll
import rospy

import sys
sys.dont_write_bytecode = True
import numpy as np
from tf.transformations import quaternion_from_euler, quaternion_matrix, quaternion_multiply, quaternion_inverse

from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension
from std_msgs.msg import String

class autonomyTasks:

    def __init__(self):

        #initialize node
        #rospy.init_node('autonomyTask', anonymous=True)

        #node subscriptions
        #self.estimatedPoseSub    = rospy.Subscriber("qubo_jetson/currentPose",     Float64MultiArray, self.pose_callback)
        #self.estimatedVelSub     = rospy.Subscriber("qubo_jetson/currentVelocity", Float64MultiArray, self.vel_callback)
        #self.estimatedGatePosSub = rospy.Subscriber("qubo_jetson/world_gate_pose", Float64MultiArray, self.gatePose_callback)

        #self.globalGateTargetPos = rospy.Publisher('qubo_jetson/globalGateTargetPos', Float64MultiArray, queue_size=1)
        #self.globalGateClosePos = rospy.Publisher('qubo_jetson/globalGateClosePos', Float64MultiArray, queue_size=1)
        #self.globalGateFarPos = rospy.Publisher('qubo_jetson/globalGateFarPos', Float64MultiArray, queue_size=1)

        passed = False

    def testTask():
        print("this task does absolutely nothing! :D")

    # 6 element current velocity vector (Qubo frame)
    # Order: x,y,z,roll,pitch,yaw
    def pose_callback(self, data):
        x = data[0]
        y = data[1]
        z = data[2]

        #parse into array
        self.quboPosition = np.array([x, y, z])

        roll  = data[3]
        pitch = data[4]
        yaw   = data[5]

        #parse into array
        self.quboOrientation = np.array([roll, pitch, yaw])
        pass

    #returns position as [x,y,z]
    def getPosition(self):

       return self.position

    #returns position as 
    def getOrientation(self):

       return self.orientation

 
    def vel_callback(self, data):
        lvelo = data[:3]
        rvelo = data[3:] 
        pass

    #parses global gate positions as [x,y,z] and [roll,pitch,yaw]
    def gatePose_callback(self, data):
        gateX = data[0]
        gateY = data[1]
        gateZ = data[2]

        self.gateGlobalPosition = np.array([gateX, gateY, gateZ])

        self.gateRoll  = data[3]
        self.gatePitch = data[4]
        self.gateYaw   = data[5]

        self.gateGlobalOrientation = np.array([self.gateRoll, self.gatePitch, self.gateYaw])

    #sets 3 global gate positions (1m in front, the gate, 1m behind)
    def setGatePositions(self):
        #positions relative to gate (to be translated into global positions)
        relativeGateClosePosition  = np.array([0, -1, 0])
        relativeGateTargetPosition = np.array([0,  0, 0])
        relativeGateFarPosition    = np.array([0,  1, 0])

        #rotation matrix for gate position
        gateOrientationQuaternion = quaternion_from_euler(self.gateGlobalOrientation[0], self.gateGlobalOrientation[1], self.gateGlobalOrientation[2])
        gateOrientationRotationMatrix = quaternion_matrix(gateOrientationQuaternion)

        #global position for 3 relative gate positions
        globalGateClosePosition  = np.add(np.matmul(gateOrientationRotationMatrix, relativeGateClosePosition), self.gateGlobalPosition)
        globalGateTargetPosition = np.add(np.matmul(gateOrientationRotationMatrix, relativeGateTargetPosition), self.gateGlobalPosition)
        globalGateFarPosition    = np.add(np.matmul(gateOrientationRotationMatrix, relativeGateFarPosition), self.gateGlobalPosition)

        globalGateTargetPosMSG = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(globalGateTargetPosition[0], globalGateTargetPosition[1], globalGateTargetPosition[2], self.gateRoll, self.gatePitch, self.gateYaw)
        )

        globalGateFarPosMSG = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(globalGateFarPosition[0], globalGateFarPosition[1], globalGateFarPosition[2], self.gateRoll, self.gatePitch, self.gateYaw)
        )

        globalGateClosePosMSG = Float64MultiArray(
            layout=MultiArrayLayout(
                dim=(MultiArrayDimension(
                    label='dim1',
                    size=6,
                    stride=6
                ),),
                data_offset=0
            ),
            data=(globalGateClosePosition[0], globalGateClosePosition[1], globalGateClosePosition[2], self.gateRoll, self.gatePitch, self.gateYaw)
        )

        self.globalGateTargetPos.publish(globalGateTargetPosMSG)
        self.globalGateFarPos.publish(globalGateFarPosMSG)
        self.globalGateClosePos.publish(globalGateClosePosMSG)
        
    def getPipePosition():
        #from sensor

        return "not implemented!"

    #returns boolean of whether or not Qubo is at a goal position to proceed to next state
    def checkStatePosition(self, goalPosition, currentPosition):

        currentTime = rospy.get_rostime()

        displacement = np.linalg.norm(goalPosition - currentPosition)

        #10 cm (0.1m) threshold
        distanceThresh = 0.15
        
        elapsedTimeLimit = 3

        #must be within 10cm

        #look into time and duration since passing threshold 
        if displacement < distanceThresh:
            return True
            if self.passed == False:
                self.passed = True
                breakTime = rospy.get_rostime()

            #time since passing within threshold
            elapsedTime = currentTime - breakTime

            #wait 3 sec to proceed
            if self.passed == True and elapsedTime.secs > elapsedTimeLimit:

                #proceed to next state
                return True

        else:
            self.passed = False


        #have not reached goalPosition
        return False

        






